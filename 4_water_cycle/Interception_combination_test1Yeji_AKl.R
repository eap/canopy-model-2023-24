# name: Yeji
# date: from Wed Dec 18
# author (in order of contributing): Yeji

# READ ME (aim of the script, equations covered, ...) ##########################
' This script aims to get intercepted water (WcAct), interacting simultaneously with Plant evaporation, 
and also the throughfall(drip), which would be used as an input to the soil moisture group. 
' 

par(mfrow = c(1, 1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################
library(base)
library(lubridate)
library(dplyr)
library(purrr)
library(ggplot2)

# READING INPUT DATA ###########################################################

## DATA WE ALL NEED #####
data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))


# Additional setup
##AKl##data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S")
data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1')   ##AKl##

# SETTING PARAMETERS ###########################################################
Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")
##AKl##data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", TZ = "Etc/GMT-1")
source(here::here("utility/setup_parameters.R"))

# aerodynamic conductance----
## defines g_ac as vector_conductance [m s-1]
##AKl##source("4_water_cycle/Max/conductance_for_moritz.R")
source("4_water_cycle/4_aerodynamic_conductance.R")   ##AKl##

# SOURCING LAI ###########################################################
source("4_water_cycle/modified_LAI.R")

#######__Values__########
G <- data_soil$G_5cm_Wm.2  # [w/m2] soil flux as value
t_C <- data_meteo$TA_degC  # temperature Celsius as variable
VPD_hPa <- data_meteo$VPD_hPa  # [hPa] vapor pressure difference
VPD <- VPD_hPa * 100  # VPD in [Pa]
P_air <- data_meteo$PRESS_hPa * 100  # [Pa] air pressure   ##AKl##
LE <- data_flux$LE_Wm.2  # [w/m2] latent heat flux for comparison
ap <- data_meteo$PRESS_hPa * 100  # air pressure in [Pa]
U <- data_meteo$WS_ms.1  # [m/s] wind speed
Lambda <- Parameters$value[Parameters$variable == "Lambda"]  # [kJ mol-1] energy flux
Lambda <- (Lambda * 1000)/(18.01/1000) # convert Lambda from kJ/mol to J/kg   ##AKl##
p_m <- Parameters$value[Parameters$variable == "p_m"]  # molar density [mol/m3]   ##AKl##
vector_conductance <- vector_conductance * p_m   # change conductance from m/s to mol/m2/s   ##AKl##

#surface resistance from Allen et al () p. 22
r_l <- 150 #bulk stomatal resistance of the well-illuminated tree [s m-1]

# Latent heat of vaporization
##AKl##latent_heat_of_vaporization <- 2466 # unit:MJ kg-1 (from lecture slide)
water_density <- 1000 # unit: kg/m^3
cp_J_mol_K <-  29.3  # J/mol/K
Lambda_J_mol <-  44.4 * 1000   # J/mol

# ACTUAL SCRIPT ################################################################
interception <- data_meteo[, c("TIMESTAMP_END", "PREC_mm")]
##AKl##interception$LAI <- get_day_LAI(data_meteo$TIMESTAMP_END)
interception$TIMESTAMP_END <- as.POSIXct(interception$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1')   ##AKl##
interception$LAI <- get_day_LAI(interception$TIMESTAMP_END, min_LAI, max_LAI)   ##AKl##
interception$WcMax <- 0.2 * interception$LAI  
# 0.2 mm per leaf
#Till: Bonan (2015) p.157

# Using a for loop to iterate through each row
# Leaves can hold the water between 0 to WcMax. The actual water holding is WcAct.
# If WcAct holds more than WcMax, then it leaves only WcMax and the rest fall through.

# Assigning them temporarily vector
interception$drip <- 0
interception$WcAct <- 0
interception$PE <- 0

# Making them into numeric values
interception$WcAct <- as.numeric(interception$WcAct)
interception$WcMax <- as.numeric(interception$WcMax)
interception$LAI <- as.numeric(interception$LAI)

#### Functions #####
# -- the saturation water vapor pressure (es) at a given temperature (t_C) --
get_es <- function(t_C) {
    6.1078 * exp((17.08085 * t_C) / (234.175 + t_C))
}
Es <- get_es(t_C) #hPa
es <- Es * 100  # convert to Pascal

# -- slope of vapor pressure----
# = the rate at which vapor pressure changes with temperature.
# approximation via Clausius-Clapeyron (Allenetal, p.37)
calculate_slope <- function(t_C) {
    sk <- (4098 * 0.6108*exp((17.27*t_C)/(t_C+237.3))) / (t_C + 237.3)^2
    #in kPa / K (in this case kPa/C° is the same as kPa/K as its a gradient)
    sp <- sk * 1000 #convert to Pa/K
    return(sp)
}
s <- calculate_slope(t_C)

# --- specific heat capacity c_p ---
##AKl##ea <- es - VPD #calculate actual vapor pressure (Pa)
ea <- es * data_meteo$RH_.  #calculate actual vapor pressure (Pa) ##AKl##
q <- (0.622 * ea) / (ap - 0.378 * ea)  # Bolton (1980) [Pa]
# q = specific humidity
# ap = atmospheric pressure.

get_cp <- function(q = NULL) {
    if (is.null(q)) {
        cp <- 1.013e3 # specific heat at constant pressure. [MJ kg-1 C-1]
    } else {
        cpd <- 1006.  # specific heat of dry air at constant pressure (J kg-1 K-1)
        cp <- cpd * (1 + 0.84 * q)
    }
    return(cp)
}

# --- Plant Evaporation ---
##AKl##pl_evapo <- function(WcAct, WcMax, s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda) {
pl_evapo <- function(WcAct, WcMax, s, Rn, G, cp, VPD, P_air, g_c, vector_conductance, Lambda) {   ##AKl##
    
    if (WcAct >= WcMax) {
        ##AKl##numerator <- s * (Rn - G) + cp * VPD * vector_conductance
        numerator <- s * (Rn - G) + cp * VPD/P_air * vector_conductance   ##AKl##
        denominator <- Lambda * s + cp
        PE <- {numerator  / denominator} 
    } else {
        ##AKl##numerator <- s * (Rn - G) + cp * VPD * vector_conductance
        numerator <- s * (Rn - G) + cp * VPD/P_air * vector_conductance   ##AKl##
        denominator <- Lambda * s + cp
        PE <- {(numerator / denominator) * (WcAct / WcMax)} 
    }
    
    return(PE)
}


# Actual PE
# WcAct = Actual water that leaves can hold
for (i in 2:nrow(interception)) { #
    # Check for missing values and non-numeric values in WcAct and WcMax
    if (!is.na(interception$WcAct[i]) && is.numeric(interception$WcAct[i]) &&
        !is.na(interception$WcMax[i]) && is.numeric(interception$WcMax[i])) {
        
          # if (i == 1) {
          #   interception$WcAct[i] <- 0
          #   interception$PE[i] <- 0
          # }
        
        # if (i > 1) {
        new_WcAct <- interception$WcAct[i - 1] + interception$PREC_mm[i]
        
        # Calculate plant evaporation (PE) using pl_evapo function only when WcAct is greater than or equal to zero
        interception$PE[i] <- ifelse(##AKl##new_WcAct >= 0,
                                     new_WcAct > 0,   ##AKl##
                                     pl_evapo(
                                         WcAct = new_WcAct,
                                         WcMax = interception$WcMax[i],
                                         ##AKl##s = calculate_slope(t_C)[i],
                                         s = calculate_slope(t_C[i]),   ##AKl##
                                         Rn = data_meteo$SW_IN_Wm.2[i] - data_meteo$SW_OUT_Wm.2[i] +
                                             data_meteo$LW_IN_Wm.2[i] - data_meteo$LW_OUT_Wm.2[i],
                                         G = data_soil$G_5cm_Wm.[i],
                                         ##AKl##cp = get_cp(q)[i],
                                         #cp = get_cp(q[i]),   ##AKl##
                                         cp = cp_J_mol_K,   ##AKl##
                                         VPD = data_meteo$VPD_hPa[i] * 100,
                                         P_air = data_meteo$PRESS_hPa[i] * 100,
                                         g_c = 1 / (rl / (interception$LAI[i] * 0.5)),
                                         # g_c = bulk surface conductance,
                                         vector_conductance = vector_conductance[i],
                                         ##AKl##Lambda = Parameters$value[Parameters$variable == "Lambda"]
                                         #Lambda   ##AKl##
                                         Lambda = Lambda_J_mol   ##AKl##
                                     ##AKl##) * cf / (latent_heat_of_vaporization * water_density),
                                     #) / cf,   ##AKl##
                                     ) * 18.01 * 0.001 * 3600,   ##AKl##
                                     0)
        
            # Update WcAct based on the previous value, precipitation, and PE
            new_WcAct <- new_WcAct - interception$PE[i]
            
            # The reason why we need new_WcAct is
            # This new value is then checked for certain conditions to ensure
            # it is non-negative and numeric before updating the WcAct in the data frame.
            
            # Check for negative and NaN values in new_WcAct
            if (!is.na(new_WcAct) && is.numeric(new_WcAct) && new_WcAct >= 0) {
                interception$WcAct[i] <- new_WcAct
                # Check conditions for WcAct and update if necessary
                if (interception$WcAct[i] > interception$WcMax[i]) {
                    # Update WcAct and calculate drip
                    interception$drip[i] <- interception$WcAct[i] - interception$WcMax[i]
                    interception$WcAct[i] <- interception$WcMax[i]
                } else {
                    interception$drip[i] <- 0 
                    interception$WcAct[i] <- interception$WcAct[i]
                }
            } else {
                # Handle negative or NaN values in WcAct
                interception$WcAct[i] <- 0
            }
        }
    }

# Save the Plant evaporation result
PE_mm_h <- interception$PE

# Yeji's own code is deleted and replaced with better code that we improved from Yeji's code

# If we assign PE as 0.1
# for (i in 1:nrow(interception)) {
#     # Check for missing values and non-numeric values in WcAct and WcMax
#     if (!is.na(interception$WcAct[i]) && is.numeric(interception$WcAct[i]) &&
#         !is.na(interception$WcMax[i]) && is.numeric(interception$WcMax[i])) {
#         
#         # Calculate plant evaporation (PE) using pl_evapo function
#         interception$PE <- 0.1 # assumption
#         
#         if (i > 1) {
#             # Update WcAct based on the previous value, precipitation, and PE
#             new_WcAct <- interception$WcAct[i - 1] + interception$PREC_mm[i] - interception$PE[i]
#             
#             # The reason why we need new_WcAct is
#             # This new value is then checked for certain conditions to ensure 
#             # it is non-negative and numeric before updating the WcAct in the data frame.
#             
#             # Check for negative and NaN values in new_WcAct
#             if (!is.na(new_WcAct) && is.numeric(new_WcAct) && new_WcAct >= 0) {
#                 interception$WcAct[i] <- new_WcAct
#                 
#                 # Check conditions for WcAct and update if necessary
#                 if (interception$WcAct[i] > interception$WcMax[i]) {
#                     # Update WcAct and calculate drip
#                     interception$drip[i] <- interception$WcAct[i] - interception$WcMax[i]
#                     interception$WcAct[i] <- interception$WcMax[i]
#                 }
#             } else {
#                 # Handle negative or NaN values in WcAct
#                 interception$WcAct[i] <- 0
#             }
#         }
#     }
# }

sum(interception$PE) # 193.8842 mm
sum(interception$PREC_mm) # 1515.2 mm
sum(interception$WcAct) # 4879.492 mm


# WRITING OUTPUT ###############################################################

# PLOTTING FIGURES #############################################################
plot(interception$TIMESTAMP_END, interception$LAI, xlab = 'Time', ylab = 'LAI', main = "Leaf Area Index", ylim = c(0, 8))
axis(2, at = seq(0, 8, by = 1))


plot(interception$TIMESTAMP_END, data_meteo$PREC_mm, ylim=c(0,2), xlab = 'Time', ylab = 'Precipitation [mm h-1]', main = 'Precipitation')

ggplot(data = interception, aes(x = TIMESTAMP_END, y = data_meteo$PREC_mm)) +
    geom_point(color = "blue", size = 2, alpha = 0.5) +
    ylim(0, 2) +
    labs(x = 'Time', y = 'Precipitation [mm h-1]', title = 'Precipitation') +
    theme(plot.title = element_text(hjust = 0.5))


plot(interception$TIMESTAMP_END, interception$PE, xlab = 'Time', ylab = 'PE', main = "Plant Evaporation")
ggplot(data = interception, aes(x = TIMESTAMP_END, y = PE)) + # Just a better plot of Plant Evaporation.
    geom_point(aes(color = ifelse(PE < 0, "Negative value of Plant Evaporation", "Plant Evaporation"))) +
    geom_hline(yintercept = 0, linetype = "dashed", color = "black") +
    labs(x = 'Time', y = 'Plant Evaporation[mm h-1]', title = "Plant Evaporation") +
    theme_minimal() +
    scale_color_manual(values = c("Plant Evaporation" = "lightblue", "Negative value of Plant Evaporation" = "turquoise3"),
                       name = "Legend") +
    theme(legend.position = "bottom",
          plot.title = element_text(hjust = 0.5))

plot(interception$TIMESTAMP_END, interception$WcAct, xlab = 'Time', ylab = 'WcAct[mm h-1]', main = "Actual water that plant can hold", ylim = c(0, 1.5), yaxp = c(0, 1.5, 15))
plot(interception$TIMESTAMP_END, interception$WcMax, xlab = 'Time', ylab = 'WcMax[mm h-1]', main = "Maximum water that plant can hold", ylim = c(0, 1.5), yaxp = c(0, 1.5, 15))


ggplot(interception, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = WcAct, linetype = "WcAct", color = "WcAct")) +
    geom_line(aes(y = WcMax, linetype = "WcMax", color = "WcMax"), size = 1.5) +
    labs(x = 'Time', y = 'Water Content [mm h-1]', title = "WcAct and WcMax Over Time") +
    scale_y_continuous(breaks = seq(0, 1.5, by = 0.1)) +
    scale_linetype_manual(values = c("WcAct" = 1, "WcMax" = 1), name = "Legend") +
    scale_color_manual(values = c("WcAct" = "blue", "WcMax" = "red"), name = "Legend") +
    theme_minimal() +
    theme(legend.position = "top", plot.title = element_text(hjust = 0.5))



ggplot(interception, aes(x = TIMESTAMP_END, y = drip)) +
    geom_point(color = "blue", size = 2, alpha = 0.5) +
    labs(x = 'Time', y = 'Throughfall [mm h-1]', title = "Throughfall Over Time") +
    theme_minimal() +
    theme(plot.title = element_text(hjust = 0.5))


# CLEAN UP #####################################################################

rm()
