# Water cycle modelling

## Description

This is the water cycle submodel of a beech stand (Measurements of Nationalpark Hainich, Germany). 

We decided to model *Plant and Soil evaporation*, total *Evapotranspiration*, *Transpiration* and *Throughfall* rates and wanted to connect the respective R scripts with each other (source them). 

![Figure 1: Conceptual model (by Till).](Supplement/conceptual_model_final3.jpg)



## Usage

### Model run


The required scripts to run the model itself can be found in the main folder:

(1) `pm_max_orderchange` calculates the **Evapotranspiration**

* for this, it sources `modified_LAI` for LAI values 

(2) `Interception_combination_test1Yeji_AKl` calculates **Interception**

* calculates Plant evaporation, throughfall(drip) simulataneously
* sources `modified_LAI` and `4_aerodynamic_conductance` /

(3) `2_Submodel_soil_evaporation` calculates the **Soil evaporation**

* for this, `3_Aerodynmic_conductance_soil` is sourced
    
(4) `Transpiration_calculations` finally calculates the **Transpiration** rates

* sources `Interception_combination_test1Yeji_AKl`, `2_Submodel_soil_evaporation`,
    and `4_aerodynamic_conductance` again

![Figure 2: Script dependencies (by Till).](Supplement/content_dependencies.jpg)


### Model evaluation 

(1) The model **calibration** and some visual **validation** is done in `validation_applied`

(2) **Sensitivity analyses**:

* for effect of LAI and air temperature on our Evapotranspiration rates, see `Sensitivity_plot`
* for effect of soil temperature, windseed and relative humidity on soil evaporation, see `Esoil_Evap_Sensitivity_tests`


Further information, such as images of the conceptual model, can be found in the [Supplement file](/Supplement) additionally. Old code versions and documents were stored in the [Old file](/old).



## Literature used in the model


Bonan, G. (2019): *Climate Change and Terrestrial Ecosystem Modeling.* Cambridge University Press. doi:10.1017/9781107339217  . Chapter  7, 9, 3.6, 15.5.

Bonan, G. (2015): *Ecological Climatology.* Cambridge University Press. doi:10.1017/CBO9781107339200  . Chapter 10, 12.

Allen, R., Pereira, L., Raes, D., & Smith, M. (1998). *FAO Irrigation and drainage paper No. 56*. Rome: Food and Agriculture Organization of the United Nations, 56, 26–40. Chapter 2 & 3.

*Readme main author: Heidi*
*Other contributors: Till*

