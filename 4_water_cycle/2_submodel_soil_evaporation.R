#name: soil evaporation
#date: Wed Dec  6 10:37:01 2023  
#authors: heidi

# READ ME (aim of the script, equations covered, ...) ##########################
'
This script calculates the soil evaporation.
Final results are stored as "Esoil_mm_h" vector (Evaporation in mm h-1).


Equation: From Bonan (2019)
# Diffusive soil evaporation equation following 7.26 
# Soil pore Water Vapour Deficit after Philips, 7.27
# Effective conductance by empirical function, 7.28
# Water Vapour deficit as proposed by AKl 

Issues:
# Matric potential assumed
# Soil surface temp given with measurement at -2 cm
# hs1 implementation not totally sure (not explained in the book clairly)
# 2 NA rows in output dataframe
'

par(mfrow=c(1,1))
Sys.setenv(LANG = "en")


# LOAD AND INSTALL LIBRARIES ###################################################

library(here)
library(tidyverse)
library(soilDB)
library(aqp)
library(latticeExtra)
library(tactile)
library(ggplot2)
library(patchwork)

# READING INPUT DATA ###########################################################
getwd()
data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))
data_profile <- read.csv(here::here("measurements/Measurements_profile_hourly_201601_201712_gapfilled.csv"))

# SETTING PARAMETERS ###########################################################
Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")
#Parameters

# create variables with the parameter values
for (i in 1:length(Parameters$variable)) {
  # varname_temp <- Parameters$variable[i]
  assign(Parameters$variable[i], Parameters$value[i])
  
}


# DEFINING FUNCTIONS ###########################################################
# assume matrix potential----
## with soil fractions estimated by AKl
## Code from Ann (WaterRententionCurve.R):

get_phi_m <- function(w_soil) {

    # create dataframe with texture data
    # soil texture data was taken from Table 1 by Kutsch et al. (2010)
    # weighted averages are used of the upper 20 cm
    soilthickness <- 20   # cm
    sand <- (5*  24 + 5*  28 + 10*  27)/20 /10   # g/kg to %
    silt <- (5* 459 + 5* 466 + 10* 473)/20 /10   # g/kg to %
    clay <- (5* 517 + 5* 506 + 10* 500)/20 /10   # g/kg to %
    BD   <- (5*0.79 + 5*0.99 + 10*1.21)/20       # bulk density in g/cm3
    SOM  <- (5*  64 + 5*  42 + 10*  24)/20       # Corg in g/kg
    SOM <- SOM * BD *soilthickness /1000 *10000 /1000       # g/kg to kg/m2
    
    x <- data.frame(sand, silt, clay, BD)

    # selecting parameters for water retention curve
    r <- ROSETTA(x, vars = c('sand', 'silt', 'clay', 'BD'))
    # deriving values for SWC and matrix potential
    vg <- KSSL_VG_model(VG_params = r, phi_min = 10^-4, phi_max=10^6) 
    
    # extract curve and add texture ID
    m <- vg$VG_curve
    
    # get matrix potential for certain SWC
    phi_kPa <- vg$VG_inverse_function((w_soil/100))   # in kPa = kJ/m3
    phi_mm <- 1000*phi_kPa / g / rho_w * 1000   # from kPa to mm
    phi_m <- (-phi_mm)/1000
    
    return(phi_m)
    
}

# Soil pore WVD factor (h_s1)----
## after Philips et al. (1978?), 7.27
## unitless
## t_surface = soil surface temperature, here at 2 cm depth [K]
## phi_m = matric potential [m], positive because defined as force
## g = gravitational constant [m s-2]
## R = universal gas constant [J K-1 mol-1]
get_hs1 <- function(t_surface, phi_m) {
  hs1 <- exp((g * (M_w/1000) * phi_m) / (R *(t_surface + 273.15)))

  return(hs1)
}

# Air pressure at 0m----
## Provided by AKl
## press = air pressure [pa]
## z = height of measurement [m]
## T_surf = soil (surface) temp at 2 cm depth [°C] 
## Rd = specific gas constant of dry air [J kg-1 K-1]
## g = gravitational constant [m s-2]

get_Pair <- function(t_surf, press) {
    z <- 44
    H <- Rd * (t_surf + 273.15) * g
    P_air_0m <- (press) / exp(-z/H)
    
    return(P_air_0m)
}

# WVD in hPa----
# from AKl (utilities.R)
## press = air pressure [pa]
## es [hPa]
## q_sat = saturated air pressure [Pa]
## T_soil = soil temp at 2 cm depth [°C]
get_qsat <- function(t_soil, press)  {
  es <- 6.1078 * exp((17.08085 * t_soil) / (234.175 + t_soil))
  es <- es * 100 # into Pa
  
  ###hPa -> mol mol-1
  P_air <- get_Pair(t_soil, press)  
  q_sat <- es / P_air # get q = e/P with P in Pa

  
  return(q_sat)
}

# q_ref----
## provided by AKl
## q_ref [Pa] = q_sat [Pa] * RH
## RH = relative humidity in 2 m height, derived from 44 m height by AKl [%]
get_qref <- function(q_sat, RH) {
    q_ref <- q_sat * (RH / 100)
}

# Effective conductance g_c----
## from Bonan (eq. 7.28, 2019)
## w_soil = soil water content [%]
## w_sat = saturated WC [%]
get_gc <- function(w_soil) {
  Se_temp <- (w_soil/100 - pwp) / (fc - pwp) 
  #Se_temp <- (w_soil/100)/w_sat #??I don't know whether it's right.. no equation in the book and cannot find the cited paper (p. 111, Bonan 2019)
  g_c <- p_m * exp(4.255 * Se_temp - 8.206) ## But results seem well (see Check outputs)
  
  return(g_c)
}

# aerodynamic conductance-----
## defines functions to calculate aerodynamic conductance [m s-1]
## as vector_conductance 
source("4_water_cycle/3_Aerodynamic_conductance_soil.R")


# ACTUAL SCRIPT ################################################################
# prepare output dataframe
output <- data.frame(
  t_h = c(),
  E = c(),
  g_c = c(),
  g_ac = c(),
  q_sat = c(),
  q_ref = c(),
  phi_m = c(),
  WVD = c()
)

## calculate matric potential beforehand to fasten up the iteration trough for-loop
results_phi_m <- -get_phi_m(data_soil$SWC_8cm_.)


## loop----
for (i in 1:nrow(data_soil)) {
  ## assign variables
  phi_m <- results_phi_m[i]
  h_s1 <- get_hs1(data_soil$TS_2cm_degC[i], phi_m)
  q_sat <- get_qsat(data_soil$TS_2cm_degC[i], data_meteo$PRESS_hPa[i]*100)
  q_ref <- get_qref(q_sat, data_profile$RH_20cm_.[i]) 
  g_c <- get_gc(data_soil$SWC_8cm_.[i])
  u_corr <- windspeedz(u = data_meteo$WS_ms.1[i])
  g_ac <- conductance(u_corr)

  ## Soil evaporation
  # Equation following 7.26 (Bonan, 2019)
  # Gives out Evaporation [mol H2O m-2 s-1]
  WVD <- (h_s1 * q_sat) - q_ref
  
  E <- (WVD/
             (g_c^(-1) + g_ac^(-1)))

  ## output file
  t_h <- data_soil$TIMESTAMP_END[i] |> 
      strptime(format = "%Y-%m-%d %H:%M:%S")

   output <- rbind(output, data.frame(E, t_h, g_c, g_ac,
                                     q_sat, q_ref,
                                     phi_m, WVD))
  
  if (i %% 1000 == 0) {print(i)} # print iteration time for handiness
}

##CHECK OUTPUTS#################################################################

#output
str(output)
which(is.na(output)) # 2 NA rows: 19610 28346

# E in mm h-1:----
## mol -> g: multiply by M_w
## g -> kg: multiply by 0.001
## s-1 -> h-1: multiply by 3600 (for hourly steps as in the measurements)

output <- output |> 
    mutate(Esoil_mm_h = E * M_w * 0.001 * 3600)

range(output$Esoil_mm_h) # 1.108246e-06 7.161415e-02
median(output$Esoil_mm_h) # 0.06
sum(output$Esoil_mm_h) # 160

Esoil_mm_h <- output$Esoil_mm_h #save as own variable for easier sourcing of this code


# summarized output----
#' check g_c: according to Bonan, g_c shall be 0.05 - 0.8 m-2 s-1

output |> 
       summarise(
        across(c(Esoil_mm_h, g_c, g_ac, q_sat, q_ref, phi_m), ~min(.x, na.rm = TRUE), .names = "min_{.col}"))
output |> 
    summarise(
        across(c(Esoil_mm_h, g_c, g_ac, q_sat, q_ref, phi_m), ~max(.x, na.rm = TRUE), .names = "max_{.col}"))
output |> 
    summarise(
        across(c(Esoil_mm_h, g_c, g_ac, q_sat, q_ref, phi_m), ~median(.x, na.rm = TRUE), .names = "median_{.col}"))

# WRITING OUTPUT ###############################################################

# save output df
# write.csv(df, here::here("Results_soil_evaporation.csv"),
#           row.names = FALSE)


# PLOTTING FIGURES #############################################################
#plot(output$WVD)
range(output$WVD) # 8.309550e-07 1.873708e-02

#### Plot: E----
plt_timeseries.E <- ggplot(data=output, aes(x=t_h)) +
    geom_line(aes(y=Esoil_mm_h)) + #, color="Soil evaporation rate")) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
    xlab('time') +
    ylab('Evaporation [mm h-1]') +
    scale_color_manual(name="", values=c("darkblue", "darkred")) +
    #theme_classic()
    theme_light() +
    ggtitle("Evaporation [mm h-1]")
plt_timeseries.E
#ggsave("plot_timeseries.png", width=10, height=6, dpi=300)
#http://127.0.0.1:40907/graphics/7ebbc63c-2566-47d6-87ba-bafc28115c26.png


#### Plot: q_sat and q_ref----

plt_timeseries.q <- ggplot(data=output, aes(x=t_h)) +
    geom_line(aes(y=q_sat, color = "Saturated")) +
    geom_line(aes(y=q_ref, color="Reference (actual)")) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
    xlab('time') +
    ylab('q (mol mol-1)') +
    scale_color_manual(name="", values=c("darkblue", "darkred")) +
    #theme_classic()
    theme_light()+
    ggtitle("Water vapour deficit (WVD)")
#plt_timeseries.q
#ggsave("plot_timeseries.png", width=10, height=6, dpi=300)


#### Plot: g_c----

plt_timeseries.g <- ggplot(data=output, aes(x=t_h)) +
    geom_line(aes(y=g_c, color = "Surface")) +
    geom_line(aes(y=g_ac, color= "Aerodynamic")) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
    xlab('time') +
    ylab('g (m-2 s-1)') +
    scale_color_manual(name="", values=c("darkblue", "darkred")) +
    #theme_classic()
    theme_light() +
    ggtitle("Conductances")

#plt_timeseries.g
#ggsave("plot_timeseries.png", width=10, height=6, dpi=300)


### plot: matric potential phi_m----

plt_timeseries.phi_m <- ggplot(data=output, aes(x=t_h)) +
    geom_line(aes(y=phi_m)) +
    #geom_line(aes(y=meas, color="observations")) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    scale_x_datetime(date_breaks="2 months", date_labels="%b\n%Y") +
    xlab('time') +
    ylab('Matric potential (m)') +
    scale_color_manual(name="", values=c("darkblue", "darkred")) +
    #theme_classic()
    theme_light()+
    ggtitle("Matric potential")
#plt_timeseries.phi_m


# Summarizing plot
(plt_timeseries.E / plt_timeseries.q)  +
(plt_timeseries.g | plt_timeseries.phi_m) +
#plot_layout(nrow = 4) +
plot_annotation("Soil evaporation submodel results") -> plot.summary

plot.summary


## monthly Evaporation----

monthly_Esoil <- output |> 
    select(t_h, Esoil_mm_h) |> 
    mutate(date = floor_date(t_h, "month")) |>
    group_by(date) |> 
    summarise(sum_E = sum(Esoil_mm_h))

ggplot(monthly_Esoil, aes(x = date, y = sum_E)) +
    geom_line() + 
    ggtitle("Monthly Soil evaporation rates [mm h-1]")

    
# CLEAN UP #####################################################################

rm()


