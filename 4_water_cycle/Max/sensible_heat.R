## sensible heat flux
data1 <- read.csv("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv")
data2 <- read.csv("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")
data3 <- read.csv("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv")

# function sensible heat flux
sensible <- function(cp,qa,ts,gah) {
    s = -cp*(qa-ts)*gah 
    return(s)
}

sensible(10, daten_sensible$potential_t, daten_sensible$data3.TS_2cm_degC, 10)
# cp = molar specific heat of most air at constant pressure ? 
# qa = potential temperature of air (adaped to surface pressure)
# ts = temperature of surface
# gah = conductance (surface and air) 
# p0 = 1000
#r/cp = 0,286
#potential temperature = ts*(po/p)^r/cp (6.9)


# daten potential temperature of air 

daten_sensible <- data.frame(data2$TIMESTAMP_END ,data2$TA_degC, data3$TS_2cm_degC, data2$PRESS_hPa)
View(daten_sensible)
daten_sensible$potential_t <- daten_sensible$data2.TA_degC*(1000/daten_sensible$data2.PRESS_hPa)^0.286
