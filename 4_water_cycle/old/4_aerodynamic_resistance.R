#name: Aerodynmaic conductance
#date: Wed Dec  6 10:37:01 2023  
#author (in order of contributing): Max and Moritz (equations and units), Heidi (layout, code refinement)

# READ ME (aim of the script, equations covered, ...) ##########################
'
This script defines a function to calculate the aerodynamic resistance in mol s-1
required for the plant evaporation subscript,
after Allen et al p. 20.
'

par(mfrow=c(1,1))
Sys.setenv(LANG = "en")


# READING INPUT DATA ###########################################################
data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))


# SETTING PARAMETERS ###########################################################
# Global parameters
Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")

# create variables with the parameter values
for (i in 1:length(Parameters$variable)) {
    # varname_temp <- Parameters$variable[i]
    assign(Parameters$variable[i], Parameters$value[i])
    
}

# DEFINING FUNCTIONS ###########################################################
resistance <- function(uz) {
    
    # adjust zom and d
    z <- 44 #measurement height
    k <- 0.4 #von Karman constant
    h <- 35 #canopy height
    
    d <- 2/3 * 35 # forest canopy
    zom <- 0.123 * 35 # forest canopy (Allen et al)
    zoh <- 0.1*zom # roughness length governing transfer of heat and vapor
    
    ra <- ((log((z-d)/zom))*(log((z-d)/zoh)))/(k^2 * uz) # m_s-1
    
    
    return(ra)
}

# ACTUAL SCRIPT ################################################################

vector_resistance <- resistance(uz = data_meteo$WS_ms.1) # not 1/... ?!?!


# CLEAN UP #####################################################################

rm()