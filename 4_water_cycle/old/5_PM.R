#name: Evapotranspiration (and plant evaporation)
#date: Wed Dec  19 11:37:01 2023  
#author (in order of contributing): Moritz

# READ ME (aim of the script, equations covered, ...) ##########################
'
equation for PM to estimate evapotranspiration E and plant evaporation (probably done in another script now)

for overview of units look at script 5_variables_explained

(includes calculation of
- net radiation Rn
- saturation vapor pressure q_sat (es)
- actual vapor pressure ea
- specific humidity q
- slope s
- bulk surface conductance g_c
- specific heat capacity c_p

uses:
- LAI calculation
- aerodynamic conductance (vector_conductance)
'

# TO-DO for later
'
- discuss plant evapo
- check units!
- replace r_l with the actual value from the leaf group (good for changing ET rate)
- do plots with time stamps
- discuss q calculation (Bolton 1980)
'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################
library(ggplot2)

# READING INPUT DATA ###########################################################
data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))

#convert measured data to required units----
    ap <- data_meteo$PRESS_hPa * 100 #air pressure in [Pa]
    G <- data_soil$G_5cm_Wm.2 # [w/m2] soil flux as value
    LE <- data_flux$LE_Wm.2 # [w/m2] latent heat flux for comparison
    Rn <- data_meteo$SW_IN_Wm.2 - data_meteo$SW_OUT_Wm.2 +
        data_meteo$LW_IN_Wm.2 - data_meteo$LW_OUT_Wm.2 #net radiation R_n [w/m2]
    t_C <- data_meteo$TA_degC # temperature Celsius as variable
    U <- data_meteo$WS_ms.1 # [m/s] wind speed
    VPD_hPa <- data_meteo$VPD_hPa # [hPa] vapor pressure difference
    VPD <- VPD_hPa*100 #VPD in [Pa]
    VPD_kPa <- VPD*1000 #VPD in [kPa]
    
# SETTING / CALCULATING PARAMETERS ###########################################################

Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")

 # create variables with the parameter values
for (i in 1:length(Parameters$variable)) {
  # varname_temp <- Parameters$variable[i]
  assign(Parameters$variable[i], Parameters$value[i])
    }

Lambda <- (Lambda * 1000)/0.018 #convert Lambda from kJ/mol to J/kg
Gamma <- Gamma * 1000 #convert Gamma to Pa/K
Gamma_kPa <- Gamma / 1000
#molar mass of water is approx. 0.018 kg/mol

#---ES (q_sat) in [Pa] (from utility -> canopy_parameters.R)
#' calculates the saturation vapour pressure, given the air temperature
#' TODO: document where this formula is coming from----
#' @param t (air) temperature in **degree Celsius**
#' @return e_s saturation vapour pressure in [Pa]
get_es <-  function(t_C)  {
  Es <-  6.1078 * exp((17.08085 * t_C) / (234.175 + t_C)) #hPa
  es <- Es * 100 #convert to Pascal Pa
  return(es)
  }
es <- get_es(t_C) #Pa

rho_a <- 1.2 #[kg m-2] simply, can later be calculated

# -- slope of vapor pressure----
# approximation via Clausius-Clapeyron (Allenetal, p.37)

calculate_slope <- function(t_C) {
    sk <- (4098 * 0.6108*exp((17.27*t_C)/(t_C+237.3))) / (t_C + 237.3)^2
    #in kPa / K (in this case kPa/C° is the same as kPa/K as its a gradient)
    sp <- sk * 1000 #convert to Pa/K
    return(sp)
}
s <- calculate_slope(t_C)
s_an <- s/1000

### --- specific heat capacity c_p ---

ea <- es - VPD #calculate actual vapor pressure (Pa)
q <- (0.622 * ea) / (ap - 0.378 * ea) #Bolton (1980) [Pa] 

#' Specific heat capacity at constant pressure (from utility -> canopy_parameters.R)
#' - according to FAO 56 guidelines cp = 1013. J kg-1 K-1
#' - according to Oncley et al. 2007, Boundary-Layer Meteorol. 123, page 16, 
#' @param q (optional) absolute air humidity in `kg m-3`
#' @return `cp` air specific heat capacity at constant pressure in `J kg-1 K-1`
get_cp <- function(q=NULL){
    
    if (is.null(q)){
        cp <- 1.013e3
    }
    else{
        cpd <- 1006.   # specific heat of dry air at constant pressure (J kg-1 K-1)
        cp <-  cpd * (1 + 0.84*q)  
    }
    return(cp)
}
cp <- get_cp(q) # in `J kg-1 K-1`

#---bulk surface conductance g_c----

#leaf are index----
#' from subscript
#' LAI stored as Vector "LAI"
source("4_water_cycle/Till/modified_LAI.R")
LAI_active <- LAI*0.5

#surface conductance (from Allen et al (equation 5) p. 22)----
r_l <- 100 #bulk stomatal resistance of the well-illuminated leaf [s m-1], later we can change this for a real value from the leaf group
r_s <- r_l/LAI_active #calculate resistance [s m-1] #big resistance -> small ET
g_c <- 1/r_s #conductance as inverted resistance [m s-1]

# --- aerodynamic conductance----
#source("4_water_cycle/Max/conductance_for_moritz.R") #old defines g_ac as vector_conductance [m s-1]
source("4_water_cycle/7_aerodynamic_conductance_new.R") #defines g_ac as vector_conductance [m s-1]

# ACTUAL SCRIPT ################################################################
mean(vector_conductance)
#old and wrong units overall evapotranspiration with vector_conductance (Bonan (2019), p.112 )
penman_monteith_max <- function(s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda)  {
    numerator <- s * (Rn - G) + cp * VPD * vector_conductance
    denominator <- s + (1 + vector_conductance/g_c) * cp / Lambda
    E <- numerator / denominator
    return(E)
}

#overall evapotranspiration new way
penman_monteith_new <- function(s, Rn, G, rho_a, cp, VPD, g_c, vector_conductance, Gamma)  {
    numerator <- s * (Rn - G) + rho_a * cp * VPD * vector_conductance
    denominator <- s + Gamma * (1 + vector_conductance/g_c)
    E <- numerator / denominator
    return(E)
}

## conversion factor----

cf <- ((2453 * 10^6))/(1000*3600) #you divide the result in W m-2 by this
# so it is divided by 2453*10^6 MJ m-3, 
#but multiplied with 3600 (seconds to hours) and 1000 m to mm

## old chapter plant evapo ----

#plant evapo     denominator <- s + (1 + (vector_conductance/g_c)) * (cp / Lambda)
#version 1## not ready yet (equation 15.13 from chapter 15.5)
#Yejis WcMax and WcAct are missing
#pl_evapo <- function(s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda)  {

#   if (WcAct >= WcMax) {
#      numerator <- s * (Rn - G) + cp * VPD * vector_conductance
#     denominator <- Lambda * s + cp
#    PE <- PE <- Lamda* (numerator / denominator)
#} else {
#numerator <- s * (Rn - G) + cp * VPD * vector_conductance
#denominator <- Lambda * s + cp
#PPE <- Lamda* (numerator / denominator)* (WcAct/WcMax)
#}
#return(PE)
#}

# WRITING OUTPUT ###############################################################

evapotranspiration <- penman_monteith_max(s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda) #2nd version with Max g_ac
et_new <- penman_monteith_new(s, Rn, G, rho_a, cp, VPD, g_c, vector_conductance, Gamma) #new with Gamma
et_an <- penman_monteith_an(s_an, Rn, G, rho_a, Cp, VPD_kPa, g_c, vector_conductance, Gamma_kPa) 
et_mm <- et_new / cf
LE_mm <- LE / cf
# PLOTTING FIGURES #############################################################

par(mfrow = c(2,2))  # plotting grid
#plot(evapotranspiration) #should be also in [W m-2]
plot(et_new) #should be also in [W m-2]
plot(LE) #[W m-2]
plot(et_mm) #[mm h-1]
plot(LE_mm) #[mm h-1]

par(mfrow = c(1, 1))  # reset


# PARAMETER CHECK 1
#par(mfrow = c(2,2))  # plotting grid

#plot(Rn) #should be also in [W m-2]
#plot(G) #[W m-2]
#plot(s) # [Pa/K]
#plot(VPD)

#par(mfrow = c(1, 1))  # reset

#plot(density(evapotranspiration), col = 'blue', main = 'Density Plot 1')
#lines(density(evapotranspiration2), col = 'red')

# CLEAN UP #####################################################################

rm()
