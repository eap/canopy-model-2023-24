#name: plant evaporation
#date: Wed Dec  20 11:37:01 2023  
#author (in order of contributing): Moritz und Max

# READ ME (aim of the script, equations covered, ...) ##########################
'
old script for plant evaporation estimate

'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# TO-DO for later
'
- use formula with cases WcAct and WcMax from Yeji
- do the plot with time stamps
'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################
library(ggplot2)
# READING INPUT DATA ###########################################################

## DATA WE ALL NEED #####

## ADDITIONAL DATA #####

#data <- read.table(here::here("4_water_cycle/..."), header=T)         #for txt in the subgroup
#data <- read.table(here::here("..."), header=T)                       #for txt in the overall project

#data <- read.csv(here::here("measurements/..."))                      #for csv in the measurements
#data <- read.csv(here::here("4_water_cycle/..."))                     #for csv in the subgroup

data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))

#######__convert measured values to values__########

G <- data_soil$G_5cm_Wm.2 # [w/m2] soil flux as value
t_C <- data_meteo$TA_degC # temperature Celsius as variable
VPD_hPa <- data_meteo$VPD_hPa # [hPa] vapor pressure difference
VPD <- VPD_hPa*100 #VPD in [Pa]
LE <- data_flux$LE_Wm.2 # [w/m2] latent heat flux for comparison
ap <- data_meteo$PRESS_hPa * 100 #air pressure in [Pa]
U <- data_meteo$WS_ms.1 # [m/s] wind speed
Lambda <- Parameters$value[Parameters$variable == "Lambda"] # [kJ mol-1] energy flux

# SETTING / CALCULATING PARAMETERS ###########################################################

Parameters <- read.csv(here::here("measurements/parameters_new.csv"), sep = ",")
Parameters
Parameters$variable # view parameters

# create variables with the parameter values
for (i in 1:length(Parameters$variable)) {
    # varname_temp <- Parameters$variable[i]
    assign(Parameters$variable[i], Parameters$value[i])
}

# WcAct and WcMax from Yeji ----

#' get_day_LAI(now()) # Get the LAI of the current moment

#' days <- as_datetime(c("2021-01-01", "2021-05-01", "2021-07-01", "2021-10-15", "2021-12-01"))
#' get_day_LAI(days)
get_day_LAI <- function(time) {
    
    stopifnot(all(is.POSIXct(time)))
    ##  --- This is some complex code to be able to support timesteps smaller than a day
    # convert the days of years in dates and the integer
    start_year <- floor_date(time, "year") #first day of the year
    pars_date <- pars %>% 
        select(leaf_out, leaf_full, leaf_fall, leaf_fall_complete) %>% 
        # convert days to seconds
        map_df(~as_datetime(.x * 86400, origin=start_year) %>% as.integer)
    time <- as.integer(time) # convert to integer for easy comparison
    ## ---
    
    case_when(
        # Winter LAI is 0
        time < pars_date$leaf_out ~ 0,
        # spring LAI has linear increase
        time >= pars_date$leaf_out & time < pars_date$leaf_full ~ {
            ndays <- pars_date$leaf_full - pars_date$leaf_out # n days of the transition
            pars$max_LAI * (time - pars_date$leaf_out) / ndays
        },
        # summer LAI is max
        time >= pars_date$leaf_full & time < pars_date$leaf_fall ~ pars$max_LAI,
        # Autumn LAI is decreasing
        time >= pars_date$leaf_fall & time < pars_date$leaf_fall_complete ~ {
            ndays <- pars_date$leaf_fall_complete - pars_date$leaf_fall # n days of the transition
            pars$max_LAI * (pars_date$leaf_fall_complete - time) / ndays
        },
        # winter again
        time >= pars$leaf_fall_complete ~ 0
    )
}

data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S")

plot(get_day_LAI(data_meteo$TIMESTAMP_END))
class(time)



# ACTUAL SCRIPT ################################################################
interception <- data_meteo[ , c("TIMESTAMP_END", "PREC_mm")]
data_meteo$TIMESTAMP_END <- as.POSIXct(data_meteo$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", TZ = "Etc/GMT-1")


interception$LAI <- get_day_LAI(data_meteo$TIMESTAMP_END)

interception$WcMax <- # 0.2*interception$PREC_mm*interception$LAI
    2*interception$LAI 

interception$WcMax <- ifelse(interception$WcMax > interception$PREC_mm, interception$PREC_mm, interception$WcMax)

interception$WcMax[2066] = 0
interception$LAI[2066] = 0
interception$WcMax[10802] = 0
interception$LAI[10802] = 0

plot(interception$WcMax)

# WcAct = Actual water that leaves can hold
# Using a for loop to iterate through each row

# Leaves can hold the water between 0 to WcMax. The actual water holding is WcAct. If WcAct holds more than WcMax, then it leaves only WcMax and the rest fall through. 

# Assumption: We will assume tree water layer can hold 20% of LAI interception. 
max_TW_hold <- interception$WcMax
plot(max_TW_hold)
# Assign temporarily value to the drip
interception$drip <- 0
interception$WcAct <- 0

# WcAct = Actual water that leaves can hold
for (i in 1:nrow(interception)) {
    # Check for missing values and non-numeric values in WcAct and WcMax
    if (!is.na(interception$WcAct[i]) && !is.na(interception$WcMax[i]) &&
        is.numeric(interception$WcAct[i]) && is.numeric(interception$WcMax[i])) {
        
        # Handle the first iteration separately
        if (i == 1) {
            interception$WcAct[i] <- interception$PREC_mm[i]
            interception$drip[i] <- 0
        } else {
            interception$WcAct[i] <- interception$WcAct[i - 1] + interception$PREC_mm[i]
            interception$drip[i] <- 0
            if (interception$WcAct[i] > max_TW_hold[i]) {
                interception$drip[i] <- interception$WcAct[i] - max_TW_hold[i]
                interception$WcAct[i] <- max_TW_hold[i]
            }
            
        }
    }
}




###__net radiation R_n ######

Rn <- data_meteo$SW_IN_Wm.2 - data_meteo$SW_OUT_Wm.2 + data_meteo$LW_IN_Wm.2 - data_meteo$LW_OUT_Wm.2

###___ES (equals q_sat)__###

#' calculates the saturation vapour pressure, given the air temperature
#' TODO: document where this formula is coming from
#' @param t (air) temperature in **degree Celsius**
#' @return e_s saturation vapour pressure in `hPa`
get_es <-  function(t_C)  {
    6.1078 * exp((17.08085 * t_C) / (234.175 + t_C))
}
Es <- get_es(t_C)
es <- Es * 100 #convert to Pascal

###__s__slope of vapor pressure ##approximation via Clausius-Clapeyron (Allenetal, p.37)

calculate_slope <- function(t_C) {
    s <- (4098 * 0.6108*exp((17.27*t_C)/(t_C+237.3))) / (t_C + 237.3)^2
    return(s) # [Pa/K]
}
s <- calculate_slope(t_C)

###__c_p__### from utilities

#' Specific heat capacity at constant pressure
#' - according to FAO 56 guidelines cp = 1013. J kg-1 K-1
#' - according to Oncley et al. 2007, Boundary-Layer Meteorol. 123, page 16, 
#' @param q (optional) absolute air humidity in `kg m-3`
ea <- es - VPD #calculate actual vapor pressure
q <- (0.622 * ea) / (ap - 0.378 * ea) #Bolton (1980) # 
#' @return `cp` air specific heat capacity at constant pressure in `J kg-1 K-1`
get_cp <- function(q=NULL){
    if (is.null(q)){
        cp <-  1.013e3
    }
    else{
        cpd <-  1006.   # specific heat of dry air at constant pressure (J kg-1 K-1)
        cp <-  cpd * (1 + 0.84*q)  
    }
    return(cp)
}
cp <- get_cp(q)
#cp <- 1013 ###if we do not have specific air humidity or formula wrong just take 1013

#######__bulk surface conductance_g_c__########

#leaf are index simple
LAI <- interception$LAI# no dimension, we first assume the maximum value
LAI_active <- LAI*0.5

#surface resistance from Allen et al () p. 22
r_l <- 100 #bulk stomatal resistance of the well-illuminated leaf [s m-1], later we can change this for a real value from the leaf group
r_s <- r_l/LAI_active #calculate resistance
g_c <- 1/r_s #conductance as inverted resistance


# DEFINING FUNCTIONS ###########################################################

#version 1## not ready yet (equation 15.13 from chapter 15.5)
#Yejis WcMax and WcAct are given above. 
pl_evapo <- function(interception, s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda)  {
    if (interception$WcAct >= interception$WcMax) {
        numerator <- s * (Rn - G) + cp * VPD * vector_conductance
        denominator <- Lambda * s + cp
        PE <- numerator / denominator}
     else {
    numerator <- s * (Rn - G) + cp * VPD * vector_conductance
    denominator <- Lambda * s + cp
    PE <- (numerator / denominator)* (WcAct/WcMax)
    }
    return(PE)

}

#easier version 2

# pl_evapo <- function(s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda)  {
  #  numerator <- s * (Rn - G) + cp * VPD * vector_conductance
   # denominator <- Lambda * s + cp
    #PE <- numerator / denominator
# return(PE)
# }

#λ: latent heat of vaporation [J kg–1]
#s: slope water pressure [???kPa/°C]
#Rn: net radiation [W/m2]
#G: Ground heat flux [W/m2]
#cp: specific heat capacity of air [J kg-1 K-1]
#VPD: water vapor deficit [Pa] (qsat(Ta): saturation vapor pressure [Pa] - 
#qa: air vapor pressure [Pa])
#vector_conductance / g_ac: aerodynamic conductance [m/s]
#g_c: surface conductance [m/s]

# ACTUAL SCRIPT ################################################################






# WRITING OUTPUT ###############################################################

plant_evapo <- pl_evapo(interception, s, Rn, G, cp, VPD, g_c, vector_conductance, Lambda) 

# PLOTTING FIGURES #############################################################

par(mfrow = c(2,2))  # plotting grid

plot(plant_evapo) #should be also in [W m-2]


par(mfrow = c(1, 1))  # reset



# CLEAN UP #####################################################################

rm()