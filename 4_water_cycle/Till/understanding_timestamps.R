#name: understand_timestamps
#date: Wed Dec  6 10:37:01 2023  
#author (in order of contributing): till

# READ ME (aim of the script, equations covered, ...) ##########################
'



'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################

library(here)
#library()

# READING INPUT DATA ###########################################################

## DATA WE ALL NEED #####


df <- read.csv(here::here("4_water_cycle/Till/A03_timestamps.csv"), stringsAsFactors=T)

## ADDITIONAL DATA #####

#data <- read.table(here::here("4_water_cycle/..."), header=T)         #for txt in the subgroup
#data <- read.table(here::here("..."), header=T)                       #for txt in the overall project

#data <- read.csv(here::here("measurements/..."))                      #for csv in the measurements
#data <- read.csv(here::here("4_water_cycle/..."))                     #for csv in the subgroup

data_flux <- read.csv(here::here("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))
data_meteo <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
data_soil <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))




# SETTING PARAMETERS ###########################################################






# DEFINING FUNCTIONS ###########################################################






# ACTUAL SCRIPT ################################################################

m <- 1
mon <- 1
df$Month <- mon   # new column added to df
for (r in 1:nrow(df)){
    if (df$Year[r]==2016){
        mon_thresh <- c(31,60,91,121,152,182,213,244,274,305,335,366)   # take leap years into account
    }else{
        mon_thresh <- c(31,59,90,120,151,181,212,243,273,304,334,365)
    }
    if (df$DoY[r]>mon_thresh[m]){
        mon <- mon +1
        m <- m +1
    }
    df$Month[r] = mon
}



# WRITING OUTPUT ###############################################################






# PLOTTING FIGURES #############################################################






# CLEAN UP #####################################################################

rm()


