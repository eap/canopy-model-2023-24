#name: convert ET/PE to mm
#date: Wed Dec  6 10:37:01 2023  
#author (in order of contributing): till

# READ ME (aim of the script, equations covered, ...) ##########################
'



'
par(mfrow=c(1,1))
Sys.setenv(LANG = "en")

# LOAD AND INSTALL LIBRARIES ###################################################

#library()
#library()


# LOAD DATA ###############################################################

source("4_water_cycle/3_Aerodynamic_conductance.R")
source("4_water_cycle/Interception.R")
source("4_water_cycle/5_PM.R")
source("4_water_cycle/2_submodel_soil_evaporation.R")

plant_evapo <- plant_evapo

evapotranspiration <- evapotranspiration

Parameters <- read.csv("measurements/parameters_new.csv", sep = ",")


for (i in 1:length(Parameters$variable)) {
    # varname_temp <- Parameters$variable[i]
    assign(Parameters$variable[i], Parameters$value[i])
    
}



# SCRIPT ################################################################

#convert from W/m^2 to mm/s

#approach: lambda [J/mol] * E [mm/h] =! ET [W/m^2]
#to get E from ET: multiply by constant for water [l/mol]



ET_mm_h <- MolH2O / 1000 * Lambda * 1000 * evapotranspiration * 3600

plot(ET_mm, type="l")
max(ET_mm)

Esoil_mm_h <- output$Esoil_mm_h
plot(Esoil_mm_h, type="l")



# WRITING OUTPUT ###############################################################






# PLOTTING FIGURES #############################################################






# CLEAN UP #####################################################################

rm()


