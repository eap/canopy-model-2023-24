#load required packages:--------
library(dplyr)
library(ggplot2)
library(readxl)
library(readr)

#read in the input data & clean it:-----
setwd("~/MSC/SoilSciences_GlobalChange/Semester 3/ecosystem atmosphere processes/Carbon_Model/canopy-model-2023-24/measurements")

#litter input data:
input_fruit <- read_excel("Measuremets_CarbonComponents_Hainich.xlsx",sheet = "NPP - Fruit")

input_leaves <- read_excel("Measuremets_CarbonComponents_Hainich.xlsx",sheet = "NPP - Wood&Leaf")

#clean the dataset Fruit:
summary(input_fruit)

#Only keep relevant columns
input_fruit <- input_fruit[10:31,1:2]

#change column description
colnames(input_fruit)[colnames(input_fruit) == "Annual Fruit Production"] <- "Year"
colnames(input_fruit)[colnames(input_fruit) == "...2"] <- "NPP_F"

# Replace "nd" with NA
input_fruit$NPP_F[input_fruit$NPP_F == "nd"] <- NA

#Now check the dataset:
summary(input_fruit)

#Change to the right mode:
input_fruit$NPP_F <- as.numeric(x = input_fruit$NPP_F)
input_fruit$Year <- as.factor(input_fruit$Year) # I defined the year as factor, since otherwise it would always add a month and day..

# Clean the dataset Leaves:
summary(input_leaves)

#Only keep relevant columns
input_leaves <- input_leaves[-1,1:5]

#change column description
colnames(input_leaves)[colnames(input_leaves) == "Total NPP"] <- "NPP"
colnames(input_leaves)[colnames(input_leaves) == "Total wood-NPP"] <- "wood_NPP"
colnames(input_leaves)[colnames(input_leaves) == "Fagus wood-NPP"] <- "fagus_wood_NPP"
colnames(input_leaves)[colnames(input_leaves) == "Leaf-NPP"] <- "leaf_NPP"

#Now check the dataset:
summary(input_leaves)

#Change to the right mode:
input_leaves$NPP <- as.numeric(x = input_leaves$NPP)
input_leaves$wood_NPP <- as.numeric(x = input_leaves$wood_NPP)
input_leaves$fagus_wood_NPP <- as.numeric(x = input_leaves$fagus_wood_NPP)
input_leaves$leaf_NPP <- as.numeric(x = input_leaves$leaf_NPP)
input_leaves$Year <- as.factor(input_leaves$Year) # I defined the year as factor, since otherwise it would always add a month and day..


#meteorological data:
data_met <-read.csv("Measurements_meteo_hourly_201601_201712_gapfilled.csv")
data_soil <- read.csv("Measurements_soil_hourly_201601_201712_gapfilled.csv")
data_resp <- read.csv("Measurements_soilresp_hourly_201601_201712.csv")
data_fluxes <- read.csv("Measurements_fluxes_hourly_201601_201712_gapfilled.csv")

#Set the initial parameter and create output df:-------
par <- read_csv("parameters.csv")

#take the k-values from the parameter dataset (c.f. Bonan p. 303)
k1 <- par$value[par$variable=="k1"] 
k2 <- par$value[par$variable=="k2"]
k3 <- par$value[par$variable=="k3"]
k4 <- par$value[par$variable=="k4"]
k5 <- par$value[par$variable=="k5"]
k6 <- par$value[par$variable=="k6"]
k7 <- par$value[par$variable=="k7"]
k8 <- par$value[par$variable=="k8"]
k9 <- par$value[par$variable=="k9"]

#for naming the a-values I used the same description as CASA-CNP model
a51 <- par$value[par$variable=="a51"]
a41 <- par$value[par$variable=="a41"]
a41 <- par$value[par$variable=="a41"]
a42 <- par$value[par$variable=="a42"]
a52 <- par$value[par$variable=="a52"]
a63 <- par$value[par$variable=="a63"]
a85 <- par$value[par$variable=="a85"]
a74 <- par$value[par$variable=="a74"] 
a76 <- par$value[par$variable=="a76"]
a75 <- par$value[par$variable=="a75"] 
a86 <- par$value[par$variable=="a86"] 
a87 <- par$value[par$variable=="a87"] 
a97 <- par$value[par$variable=="a97"] 
a98 <- par$value[par$variable=="a98"] 


#read out the values for the scaling factors: 
S1 <- par$value[par$variable=="S1"]
S2 <- par$value[par$variable=="S2"]
S3 <- par$value[par$variable=="S3"]
S4 <-par$value[par$variable=="S4"] 
S5 <- par$value[par$variable=="S5"] 
S6 <- par$value[par$variable=="S6"] 
S7 <- par$value[par$variable=="S7"] 
S8 <- par$value[par$variable=="S8"] 
S9 <- par$value[par$variable=="S9"] 

#Here is space for the temperature and moisture function:-----


###calculate weighted average moisture and temperature for the profile (30cm)
soil_filtered <- data_soil %>%
    #create new columns based on row-wise means of multiple columns
    mutate(
        TS_profile = (TS_2cm_degC*2/30 + TS_5cm_degC*3/30 + TS_15cm_degC*10/30 + TS_30cm_degC*15/30),
        SWC_profile = (2*SWC_8cm_.*0.08 + (SWC_8cm_. + SWC_16cm_.) * 0.16 + (SWC_16cm_. + SWC_32cm_.) * 0.3) / (2 * (0.08 + 0.16 + 0.3))
    ) %>%
    #select only these columns for new dataframe
    select(TIMESTAMP_END, TS_profile, SWC_profile)


#check the df:
summary(soil_filtered)

#assign time-format to first column:
soil_filtered$TIMESTAMP_END <- as.POSIXct(soil_filtered$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S")

# SE function (=pore space filled with water)

# equation based on equation 18.13 in Bonan (chapter 18)
# SE --> SWC: volumetric WC, soil porosity 
#-->  fraction of the volume of voids over the total volume --> Porosity = (1 - (Bulk Density ÷ Particle Density)) x 100
# function according to original source Kelly et al. 2000
SE_function <- function(soil_filtered){
    soil_filtered$SE <- numeric(nrow(soil_filtered))
    for (i in 1:nrow(soil_filtered)){
        soil_filtered$SE[i] <- soil_filtered$SWC_profile[i] / ((1 - (0.89 / 2.65)) * 100)
    }
    return(soil_filtered)
}
# 15: thickness soil layer; 0.89: mean bulk density of 10cm soil layer (Knautschtal et al, 2010); 
#2.65: particle density --> constant

# a, b,c  and d depend on soil texture --> here use of values for fine soil texture as soil in Hainich 
#is described as silt-clayey
a <- 0.6
b <- 1.27
c <- 0.0012
d <- 2.84
e <- d * (b - a) / (a - c)

SWC_function <- function(soil_filtered, a, b, c, d, e){
    soil_filtered$SWC_factor <- numeric(nrow(soil_filtered))
    for (i in 1:nrow(soil_filtered)) {
        soil_filtered$SWC_factor[i] <- ((soil_filtered$SE[i] - b) / (a - b)) ^ e * ((soil_filtered$SE[i] - c) / (a - c)) ^ d
    }
    return(soil_filtered)
}

soil_filtered <- SE_function(soil_filtered) #(=pore space filled with water)
soil_filtered <- SWC_function(soil_filtered, a=a, b=b,c=c,d=d,e=e)


###Now do temperature and add it to the moisture DF:

#define input:
celsius <- soil_filtered$TS_profile

##For both model types use temperature in Kelvin!! Transfer °C to K:
kelvin <- celsius + 273.15

temperature_function <- function(Temp, model){
    if (model == "CASA"){
        temperature <- exp(308.56*((1/56.02)-(1/(Temp-227.13))))
    } 
    else if (model == "CENTURY"){ # atan(x) is the command for arctangant 
        temperature <- 0.56 + (1.46/pi)*atan(0.031*pi*(Temp-288.85))
    } else { #this is for weirdos that try even other models
        print("Temperature function not applicable")
    }
    return(temperature=temperature)
}

#now run the function and add the results to the data frame "soil_filtered":

temp_factor_casa <- temperature_function(Temp=kelvin, model="CASA")
temp_factor_century <-temperature_function(Temp=kelvin, model="CENTURY") 

#add the results to the table:
soil_filtered <- cbind(soil_filtered, temp_factor_casa, temp_factor_century)
##soil_filtered is the finished input vector for our SoilCmodel function

#creating new columns to store the results 

soil_filtered$meta <- 0
soil_filtered$struc <- 0 
soil_filtered$CWD <- 0 
soil_filtered$fastSOM <- 0 
soil_filtered$slowSOM <- 0
soil_filtered$RESP <- 0


# setting initial values for carbon pools
soil_filtered[1, "meta"] <- 1200 # value from metabolic litter
soil_filtered[1, "struc"] <- 400 # value from structural litter
soil_filtered[1, "fastSOM"] <- 800 #value from fast SOM
soil_filtered[1, "slowSOM"] <- 6000 #value from slow SOM
soil_filtered[1, "CWD"] <- 2400 #value from CWD



# litter input leaves and fruit:--------------
input_fruit_total_2016 <- 202.8 # NPP fruits (gC/m2)
input_fruit_total_2017 <- 11.0

input_leaves_total_2016 <- 157 # Leaf-NPP g C  m-2 yr-1
input_leaves_total_2017 <- mean(input_leaves$leaf_NPP)

# distributing litter input leaves +fruits over 20 days in October
for(j in 1:nrow(soil_filtered)){
    soil_filtered$input_leaves_hourly <- ifelse(
        soil_filtered$TIMESTAMP_END >= as.POSIXct("2016-10-07 00:00:00") & soil_filtered$TIMESTAMP_END <= as.POSIXct("2016-10-27 23:00:00"),
        (input_leaves_total_2016 / 480), #yearly input divided by 480 --> 20 days * 24 hours = 480
        ifelse(
            soil_filtered$TIMESTAMP_END >= as.POSIXct("2017-10-07 00:00:00") & soil_filtered$TIMESTAMP_END <= as.POSIXct("2017-10-27 23:00:00"),
            (input_leaves_total_2017 / 480),
            0
        )
    )
    soil_filtered$input_fruits_hourly <- ifelse(
        soil_filtered$TIMESTAMP_END >= as.POSIXct("2016-10-07 00:00:00") & soil_filtered$TIMESTAMP_END <= as.POSIXct("2016-10-27 23:00:00"),
        (input_fruit_total_2016 / 480),
        ifelse(
            soil_filtered$TIMESTAMP_END >= as.POSIXct("2017-10-07 00:00:00") & soil_filtered$TIMESTAMP_END <= as.POSIXct("2017-10-27 23:00:00"),
            (input_fruit_total_2017 / 480),
            0
        )
    )
}


###There is 2 NA values in the timestamp vector, which leads to NA values in the input values. Therefore replace the NA values with 0 in order to keep the loop stable:
soil_filtered$input_leaves_hourly[is.na(soil_filtered$input_leaves_hourly)] <- 0
soil_filtered$input_fruits_hourly[is.na(soil_filtered$input_fruits_hourly)] <- 0

#function for CASA normal given initial values ----------
SoilCfunction_own_scaling <- function(time){
    
    simulation <- soil_filtered
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- simulation$temp_factor_casa[i]*simulation$SWC_factor[i]
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

##now make a testrun!!
testrun <- SoilCfunction_own_scaling(time = (length(soil_filtered$TIMESTAMP_END)-1))

#converting Respiration units to µmolC / m2 / s
testrun$RESP_mol <- testrun$RESP * 23.14814815


##Plot the respiration output normal given initial values:-----------
library(tidyverse)
library(ggplot2)
library(lubridate)
library(zoo)


testrun$Resp_obs_umolm <- data_resp$Rsoil_exp_umolm.2s.1

ggplot(data=testrun, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulations"), size=0.7) +
    # Measurements
    geom_line(aes(y=Resp_obs_umolm, color="observations"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor, given initial values") +
    theme_light()

#### the same but with new initial values after 100 years loop with S factor!:----------------

## set new initial values:

soil_filtered[1, "meta"] <- 118.4511 # value from metabolic litter
soil_filtered[1, "struc"] <- 281.9292 # value from structural litter
soil_filtered[1, "fastSOM"] <- 227.2797 #value from fast SOM
soil_filtered[1, "slowSOM"] <- 2058.305 #value from slow SOM
soil_filtered[1, "CWD"] <- 702.3216 #value from CWD



SoilCfunction_own_scaling <- function(time){
    
    simulation <- soil_filtered
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- simulation$temp_factor_casa[i]*simulation$SWC_factor[i]
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

##now make a testrun!!
testrun <- SoilCfunction_own_scaling(time = (length(soil_filtered$TIMESTAMP_END)-1))

#converting Respiration units to µmolC / m2 / s
testrun$RESP_mol <- testrun$RESP * 23.14814815


##Plot the respiration output new initial values with S-factor:-----------


testrun$Resp_obs_umolm <- data_resp$Rsoil_exp_umolm.2s.1

ggplot(data=testrun, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulations"), size=0.7) +
    # Measurements
    geom_line(aes(y=Resp_obs_umolm, color="observations"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor, calculated initial values (100 years loop)") +
    theme_light()

#calculate the rmse:
rmse <- sqrt(mean((testrun$Resp_obs_umolm - testrun$RESP_mol)^2, na.rm = TRUE)) # unit: umol/m2/s
print(rmse)
#1.770388



#### the same but with new initial values after 100 years loop with own values!----
soil_filtered[1, "meta"] <- 54.60 # value from metabolic litter
soil_filtered[1, "struc"] <- 129.92 # value from structural litter
soil_filtered[1, "fastSOM"] <- 103.22 #value from fast SOM
soil_filtered[1, "slowSOM"] <- 899.98 #value from slow SOM
soil_filtered[1, "CWD"] <- 305.51 #value from CWD

soil_filtered$leaf <- 0
soil_filtered$fruits <- 0 



SoilCfunction_own_scaling <- function(time){
    
    simulation <- soil_filtered
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- simulation$temp_factor_casa[i]*simulation$SWC_factor[i]
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

##now make a testrun!!
testrun_init <- SoilCfunction_own_scaling(time = (length(soil_filtered$TIMESTAMP_END)-1))

#converting Respiration units to µmolC / m2 / s
testrun_init$RESP_mol <- testrun_init$RESP * 23.14814815

##Plot the respiration output new initial values with own scaling:-----------


testrun$Resp_obs_umolm <- data_resp$Rsoil_exp_umolm.2s.1

ggplot(data=testrun, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulations"), size=0.7) +
    # Measurements
    geom_line(aes(y=Resp_obs_umolm, color="observations"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor, calculated initial values (100 years loop)") +
    theme_light()

#calculate the RMSE:
#calculate the rmse:
rmse <- sqrt(mean((testrun$Resp_obs_umolm - testrun$RESP_mol)^2, na.rm = TRUE)) # unit: umol/m2/s
print(rmse)



#function for CASA sensitivity analysis: increase the scaling factor by *2 ----------
SoilCfunction_own_scaling <- function(time){
    
    simulation <- soil_filtered 
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- (simulation$temp_factor_casa[i]*simulation$SWC_factor[i])*2
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

##now make a testrun!!
testrun_up <- SoilCfunction_own_scaling(time = (length(soil_filtered$TIMESTAMP_END)-1))

#converting Respiration units to µmolC / m2 / s
testrun_up$RESP_mol <- testrun_up$RESP * 23.14814815


##Plot the respiration output with increased scaling factor by 2:-----------

testrun_up$Resp_obs_umolm <- data_resp$Rsoil_exp_umolm.2s.1

ggplot(data=testrun_up, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulations"), size=0.7) +
    # Measurements
    geom_line(aes(y=Resp_obs_umolm, color="observations"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor multiplied by 2, calculated initial values") +
    theme_light()

#calculate the rmse:
rmse <- sqrt(mean((testrun_up$Resp_obs_umolm - testrun_up$RESP_mol)^2, na.rm = TRUE)) # unit: umol/m2/s
print(rmse)



#function for CASA sensitivity analysis: decrease the scaling factor by *2 ----------
SoilCfunction_own_scaling <- function(time){
    
    simulation <- soil_filtered 
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- (simulation$temp_factor_casa[i]*simulation$SWC_factor[i])*0.5
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

##now make a testrun!!
testrun_down <- SoilCfunction_own_scaling(time = (length(soil_filtered$TIMESTAMP_END)-1))

#converting Respiration units to µmolC / m2 / s
testrun_down$RESP_mol <- testrun_down$RESP * 23.14814815


##Plot the respiration output with decrease scaling factor by 2:-----------

testrun_down$Resp_obs_umolm <- data_resp$Rsoil_exp_umolm.2s.1

ggplot(data=testrun_down, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulations"), size=0.7) +
    # Measurements
    geom_line(aes(y=Resp_obs_umolm, color="observations"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor multiplied by 0.5, calculated initial values") +
    theme_light()

### Now calculate an RSME:

rsme_down <- sqrt(mean((testrun_down$Resp_obs_umolm - testrun_down$RESP_mol)^2, na.rm = TRUE)) # unit: umol/m2/s

rsme_down 


#### make an automatically function of nelder mead:-----------
#####First define k in te function:
SoilCfunction_own_scaling <- function(time,k){
    
    simulation <- soil_filtered 
    #defining the first input step (not sure if this is neccessary after step above)
    simulation$leaf[1] <- simulation$input_leaves_hourly[1]
    simulation$fruits[1] <- simulation$input_fruits_hourly[1]
    
    for (i in 1:(time)) { 
        
        scaling <- (simulation$temp_factor_casa[i]*simulation$SWC_factor[i])*k
        
        simulation$leaf[i+1] <- simulation$leaf[i] + simulation$input_leaves_hourly[i+1] - k1*scaling*simulation$leaf[i]
        
        simulation$fruits[i+1] <- simulation$fruits[i] + simulation$input_fruits_hourly[i+1] - k3*scaling*simulation$fruits[i]
        
        simulation$meta[i+1] <- simulation$meta[i] + a41*k1*scaling*simulation$leaf[i] - k4*scaling*simulation$meta[i]
        
        simulation$CWD[i+1] <- simulation$CWD[i] + a63*k3*scaling*simulation$fruits[i] - k6*scaling*simulation$CWD[i]
        
        simulation$struc[i+1] <- simulation$struc[i] + a51*k1*scaling*simulation$leaf[i] - k5*scaling*simulation$struc[i]
        
        simulation$fastSOM[i+1] <- simulation$fastSOM[i] + k4*scaling*a74*simulation$meta[i] + k5*scaling*a75*simulation$struc[i] + a76*k6*scaling*simulation$CWD[i] - k7*scaling*simulation$fastSOM[i]
        
        simulation$slowSOM[i+1] <- simulation$slowSOM[i] + k7*scaling*a87*simulation$fastSOM[i] + k5*scaling*a85*simulation$struc[i] + a86*k6*scaling*simulation$CWD[i]- k8*scaling*simulation$slowSOM[i]
        
        simulation$RESP[i+1] <- sum(simulation$meta[i]*k4*scaling*(1-a74) + simulation$struc[i]*k5*scaling*(1-a85-a75) + simulation$CWD[i]*k6*scaling*(1-a76-a86) + simulation$fastSOM[i]*k7*scaling*(1-a87-a97) + simulation$slowSOM[i]*k8*scaling*(1-a98))
    }
    return(simulation)
}

library(nloptr)


# run Nelder-Mead Simplex to minimize RMSE
    
    # Define initial parameter values
    k_initial <- 2  # Provide initial values for parameters
    
    # Objective function to minimize RMSE
    obj_crit <- function(x) {
        # meas of SOC
        mon <- testrun$TIMESTAMP_END
        Resp_obs <- testrun$Resp_obs_umolm
        obs <- as.data.frame(cbind(mon, Resp_obs))
        
        # Initial values
        k_i <- x
        time_i <- (length(testrun$TIMESTAMP_END) - 1)
        
        # Model run
        mod_output <- SoilCfunction_own_scaling(time = time_i, k = k_i)
        mod_output$RESP_mol <- mod_output$RESP * 23.14814815
        
        # Comparison with observations
        sim <- mod_output$RESP_mol
        rmse <- sqrt(mean((obs$Resp_obs - sim)^2, na.rm=TRUE))
        
        return(rmse)
    }
    
    
    # Run Nelder-Mead Simplex to minimize RMSE
    mod_calib <- neldermead(x0=k_initial, fn=obj_crit, lower=1, upper = 10)
    
    mod_calib #Results for given initial values:
    
    # $par
    # [1] 4.361445
    # 
    # $value
    # [1] 1.056575
    # 
    # $iter
    # [1] 42
    # 
    # $convergence
    # [1] 4
    # 
    # $message
    # [1] "NLOPT_XTOL_REACHED: Optimization stopped because xtol_rel or xtol_abs (above) was reached."
    
    
    ######  
    
    
    # Results for the new initial values with Sfactor:
    #  $par
    # [1] 3.280577
    # 
    # $value
    # [1] 1.028075
    # 
    # $iter
    # [1] 40
    # 
    # $convergence
    # [1] 4
    # 
    # $message
    # [1] "NLOPT_XTOL_REACHED: Optimization stopped because xtol_rel or xtol_abs (above) was reached."
    # 

 #### this is the results for the new initial values calculated with our own scaling factor:
    # $par
    # [1] 8.14867
    # 
    # $value
    # [1] 1.5277
    # 
    # $iter
    # [1] 44
    # 
    # $convergence
    # [1] 4
    # 
    # $message
    # [1] "NLOPT_XTOL_REACHED: Optimization stopped because xtol_rel or xtol_abs (above) was reached."
    
    
    
# plot results of model calibration
# initial values

k <- mod_calib$par
time <- (length(testrun$TIMESTAMP_END) - 1)

# model run
mod_output <- SoilCfunction_own_scaling(time,  k)
mod_output$RESP_mol <- mod_output$RESP * 23.14814815
mod_output$RESP_mol_old <- testrun_init$RESP_mol


# plot
ggplot(data=mod_output, aes(x=TIMESTAMP_END)) +
    # Model
    geom_line(aes(y=RESP_mol, color="simulation new"), size=0.7) +
    # Measurements
    geom_line(aes(y=obs$Resp_obs, color="observations"), size=0.7) +
    geom_line(aes(y=mod_output$RESP_mol_old, color="simulation old"), size=0.7) +
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    xlab('time') +
    ylab('Soil respiration (µmol CO2 / m2 /s)') +
    scale_color_manual(name="", values = c("darkblue", "darkred", "darkgrey")) +
    labs(title = "Observed respiration at Hainich Forest vs simulated", subtitle = "Settings: own scaling factor multiplied by 8.14867, RMSE: 1.028075, calculated initial values (100 years loop)") +
    theme_light()

#add observations to dataset
mod_output$Resp_obs <- obs$Resp_obs

#scatter plot observations vs. simulated values
ggplot(data=mod_output) +
    geom_point(aes(x=RESP_mol, y=Resp_obs), size=1, color='grey30', alpha=.5) +
    geom_smooth(aes(x=RESP_mol, y=Resp_obs), method="lm", fullrange=TRUE, color="red") +      # adding regression line
    geom_hline(yintercept=0, linetype="dashed", color="black") +
    geom_vline(xintercept=0, linetype="dashed", color="black") +
    geom_abline(slope=1, linetype="dashed", color="black") +
    xlab('simulated variable (unit)') +
    ylab('observed variable (unit)') +
    theme_light() +
    theme(aspect.ratio=1, legend.position="none")


