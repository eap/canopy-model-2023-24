library(here)

#### Open Data sets ####
# Name and View 

# parameters <- read.csv2(here::here ("Leaf_temperature_model_charlotte_martha/measurements/parameters_new.csv"))
# 
# meteo <- read.csv2(here::here ("Leaf_temperature_model_charlotte_martha/measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))
#  
# fluxes <- read.csv2(here::here ("Leaf_temperature_model_charlotte_martha/measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))

parameters <- read.csv(here::here ("measurements/parameters_new.csv"))

meteo <- read.csv(here::here ("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv"))

fluxes <- read.csv(here::here ("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv"))

# Merge the data sets "meteo" and "fluxes" by timestamps

df <- merge(meteo, fluxes, by = "TIMESTAMP_END", sort=F)

# Delete unnecessary columns from dataframe
library(dplyr)
df <- df %>% select(-PPFD_IN_umolm.2s.1, -PPFD_OUT_umolm.2s.1, -PREC_mm, -WD_deg, -CO2conc_ppm, 
                    -NEE_umolm.2s.1, -GPP_DT_umolm.2s.1, -RECO_DT_umolm.2s.1, -NEE_DT_umolm.2s.1)

#### Leaf temperature (K) #####

calc_Tl <- as.numeric(df$TA_degC) + 273.15 + 1 # to convert degree celsius in calvin +1
df[,"Tl_K"] <- calc_Tl

#### Leaf temperature (Degree Celcius) #####

Tl_DC <- as.numeric(df$Tl_K) -273.15 # to estimate the leaf temperature in degree celcius
df[, "Tl_DC"] <- Tl_DC

#### Air temperature (K) ####

calc_Ta <- as.numeric(df$TA_degC) + 273.15 # to convert degree celsius in calvin
df[,"Ta_K"] <- calc_Ta

#### Calculate leaf boundary layer conductance ####

Ta <- as.numeric(df$Ta_K) # Air temperature (K)
P <-  as.numeric(df$PRESS_hPa)*100 # Air pressure (Pa) Von hPa in Pa *100
WS <- as.numeric (df$WS_ms.1) # Wind speed (m/s)
Tl <- as.numeric(df$Tl_K) # Leaf temperature (K)
d <- 0.05 # Leaf dimension (m)

calc_gbh <- function(Ta, P, WS, Tl, d=0.05){
  
  # Physical constants
  Tfrz <- 273.15      # Freezing point of water (K)
  g <- 9.80616        # Gravitational acceleration (m/s2)
  Rgas <- 8.31447     # Universal gas constant (J/K/mol)
  visc0 <- 13.3e-06   # Kinematic viscosity at 0 degC and 1013.25 hPa (m2/s)
  Dh0 <- 18.9e-06     # Molecular diffusivit y (heat) at 0 degC and 1013.25 hPa (m2/s)
  Dv0 <- 21.8e-06     # Molecular diffusivity (H2O) at 0 degC and 1013.25 hPa (m2/s)
  
  # Input variables
  rhomol <- numeric() # Air density
  rhomol <- P / (Rgas * Ta)   # Molar density (mol/m3)
  
  Re <- numeric() # Re = Reynolds number
  
  gbh <- numeric()   # leaf boundary layer conductance for heat (mol/m2/s)
  #gbw <- numeric()   # leaf boundary layer conductance for water vapor (mol/m2/s)
  
  # Adjust diffusivity for temperature and pressure
  # following Tab. A.3, p. 381
  # fac = Calculated diffusion coefficient for a given temperature and pressure
  fac <- 101325 / P * (Ta / Tfrz)^1.81
  
  visc <- visc0 * fac   # Kinematic viscosity (m2/s)
  Dh <- Dh0 * fac       # Molecular diffusivity, heat (m2/s)
  Dv <- Dv0 * fac       # Molecular diffusivity, H2O (m2/s)
  
  # Dimensionless numbers
  Re <- WS * d / visc                              # Reynolds number, eq. 10.32
  Pr  <- visc / Dh                                       # Prandtl number, eq. 10.31
  Scv <- visc / Dv                                       # Schmidt number for H2O, eq. 10.37
  Gr <- g * d^3 * (Tl - Ta) / (Ta * visc^2)   # Grashof number, eq. 10.42
  #  print(c(Re, Gr))
  
  
  b1 <- 1.5                              # Empirical correction factor for Nu and Sh
  
  if (Gr/(Re^2) < 1){   # ratio of buoyancy to inertial forces
    
    if (Re < 2100){
      # Forced convection - laminar flow
      Nu  <- b1 * 0.66 *  Pr^0.33 * Re^0.5   # Nusselt number, eq. 10.33
      Shv <- b1 * 0.66 * Scv^0.33 * Re^0.5   # Sherwood number, H2O, eq. 10.38
      flag <- 1                              # indicator for forced convection, laminar flow
    }else{
      # Forced convection - turbulent flow
      Nu  <- b1 * 0.036 *  Pr^0.33 * Re^0.8   # Nusselt number, eq. 10.40
      Shv <- b1 * 0.036 * Scv^0.33 * Re^0.8   # Sherwood number, H2O, eq. 10.41
      flag <- 2                               # indicator for forced convection, turbulent flow
    }
    
  }else{
    # Free convection
    if (Gr < 10^5){
      Nu  <- 0.54 *  Pr^0.25 * Gr^0.25   # Nusselt number, eq. 10.43
      Shv <- 0.54 * Scv^0.25 * Gr^0.25   # Sherwood number, H2O, eq. 10.44
    }else{
      Nu  <- 0.15 *  Pr^0.33 * Gr^0.33   # Nusselt number, eq. 10.45
      Shv <- 0.15 * Scv^0.33 * Gr^0.33   # Sherwood number, H2O, eq. 10.46
    }
    flag <- 3                              # indicator for free convection, laminar flow
  }
  
  
  gbh <- Dh * Nu / d * rhomol    # Boundary layer conductance, heat (mol/m2/s), eq. 10.30
  #gbw <- Dv * Shv / d * rhomol   # Boundary layer conductance, H2O (mol/m2/s), eq. 10.39 & 10.30
  
  
  #results <- data.frame("gbh"=gbh, "gbw"=gbw, "flag"=flag)
  results_gbh <- ("gbh" = gbh)
  return(gbh)
}

#### Calculate leaf boundary layer conductance ####

Ta <- as.numeric(df$Ta_K) # Air temperature (K)
P <-  as.numeric(df$PRESS_hPa)*100 # Air pressure (Pa) Von hPa in Pa *100
WS <- as.numeric (df$WS_ms.1) # Wind speed (m/s)
Tl <- as.numeric(df$Tl_K) # Leaf temperature (K)
d <- 0.05 # Leaf dimension (m)

calc_gbw <- function(Ta, P, WS, Tl, d=0.05){
  
  # Physical constants
  Tfrz <- 273.15      # Freezing point of water (K)
  g <- 9.80616        # Gravitational acceleration (m/s2)
  Rgas <- 8.31447     # Universal gas constant (J/K/mol)
  visc0 <- 13.3e-06   # Kinematic viscosity at 0 degC and 1013.25 hPa (m2/s)
  Dh0 <- 18.9e-06     # Molecular diffusivit y (heat) at 0 degC and 1013.25 hPa (m2/s)
  Dv0 <- 21.8e-06     # Molecular diffusivity (H2O) at 0 degC and 1013.25 hPa (m2/s)
  
  # Input variables
  rhomol <- numeric() # Air density
  rhomol <- P / (Rgas * Ta)   # Molar density (mol/m3)
  
  Re <- numeric() # Re = Reynolds number
  
  #gbh <- numeric()   # leaf boundary layer conductance for heat (mol/m2/s)
  gbw <- numeric()   # leaf boundary layer conductance for water vapor (mol/m2/s)
  
  # Adjust diffusivity for temperature and pressure
  # following Tab. A.3, p. 381
  # fac = Calculated diffusion coefficient for a given temperature and pressure
  fac <- 101325 / P * (Ta / Tfrz)^1.81
  
  visc <- visc0 * fac   # Kinematic viscosity (m2/s)
  Dh <- Dh0 * fac       # Molecular diffusivity, heat (m2/s)
  Dv <- Dv0 * fac       # Molecular diffusivity, H2O (m2/s)
  
  # Dimensionless numbers
  Re <- WS * d / visc                              # Reynolds number, eq. 10.32
  Pr  <- visc / Dh                                       # Prandtl number, eq. 10.31
  Scv <- visc / Dv                                       # Schmidt number for H2O, eq. 10.37
  Gr <- g * d^3 * (Tl - Ta) / (Ta * visc^2)   # Grashof number, eq. 10.42
  #  print(c(Re, Gr))
  
  
  b1 <- 1.5                              # Empirical correction factor for Nu and Sh
  
  if (Gr/(Re^2) < 1){   # ratio of buoyancy to inertial forces
    
    if (Re < 2100){
      # Forced convection - laminar flow
      Nu  <- b1 * 0.66 *  Pr^0.33 * Re^0.5   # Nusselt number, eq. 10.33
      Shv <- b1 * 0.66 * Scv^0.33 * Re^0.5   # Sherwood number, H2O, eq. 10.38
      flag <- 1                              # indicator for forced convection, laminar flow
    }else{
      # Forced convection - turbulent flow
      Nu  <- b1 * 0.036 *  Pr^0.33 * Re^0.8   # Nusselt number, eq. 10.40
      Shv <- b1 * 0.036 * Scv^0.33 * Re^0.8   # Sherwood number, H2O, eq. 10.41
      flag <- 2                               # indicator for forced convection, turbulent flow
    }
    
  }else{
    # Free convection
    if (Gr < 10^5){
      Nu  <- 0.54 *  Pr^0.25 * Gr^0.25   # Nusselt number, eq. 10.43
      Shv <- 0.54 * Scv^0.25 * Gr^0.25   # Sherwood number, H2O, eq. 10.44
    }else{
      Nu  <- 0.15 *  Pr^0.33 * Gr^0.33   # Nusselt number, eq. 10.45
      Shv <- 0.15 * Scv^0.33 * Gr^0.33   # Sherwood number, H2O, eq. 10.46
    }
    flag <- 3                              # indicator for free convection, laminar flow
  }
  
  
  #gbh <- Dh * Nu / d * rhomol    # Boundary layer conductance, heat (mol/m2/s), eq. 10.30
  gbw <- Dv * Shv / d * rhomol   # Boundary layer conductance, H2O (mol/m2/s), eq. 10.39 & 10.30
  
  
  #results <- data.frame("gbh"=gbh, "gbw"=gbw, "flag"=flag)
  results_gbw <- ("gbw" = gbw)
  return(gbw)
}

#### Loop gbh per hour ####

gbh<- NA # You need this that's it no explanation

for (h in 1:nrow(df)){
  results_gbh <- calc_gbh(Ta[h], P[h], WS[h], Tl[h], d=0.05) # Loop for gbh per hour
  # d=0.05 is a constant, therefore not by hour
  gbh[h] <- results_gbh
}

df[,"calc_gbh"] <- gbh

#### Loop for gbw per hour ####

gbw<- NA 

for (h in 1:nrow(df)){
  results_gbw <- calc_gbw(Ta[h], P[h], WS[h], Tl[h], d=0.05) # Loop for gbw per hour
  # d=0.05 is a constant, therefore not by hour
  gbw[h] <- results_gbw
}

df[,"calc_gbw"] <- gbw

#### Sensible heat flux of leaf (W*m−2): Fick's Law (Bonan eq.10.5)####

Cp <- 29.3    # Specific heat capacity of air (J*mol-1*K-1)
Tl <- as.numeric(df$Tl_K) # Leaf surface temperature (K)
Ta <- as.numeric(df$Ta_K) # Air temperature (K)
gbh <- as.numeric(df$calc_gbh) # Boundary layer conductance for heat (mol*m-2*s-1)

calc_H <- function (Tl, Ta, gbh, Cp=29.3){
  
  # Calculate the Sensible heat flux
  # H <- 2*Cp*(Tl-Ta)*gbh
  H <- 2* Cp*(Tl-Ta)*gbh # Because it has only one side considered ?????????????????????????????????????????????????
  
  results_H <- ("H"= H)
  return(H)
}

#### Loop H per hour ####

H <- NA
for (h in 1:nrow(df)) {
  results_H <- calc_H (Tl[h], Ta[h], gbh[h], Cp=29.3) # Loop for H per hour
  H[h] <- results_H
}
df[, "H"] <- H

#### Latent heat flux of leaf (W*m−2) ####

VPD_Pa <- as.numeric(df$VPD_hPa)*100 # Vapor pressure deficit (Pa)
df[,"VPD_Pa"] <- VPD_Pa

VPD_Pa <- as.numeric(df$VPD_Pa) # Vapor pressure deficit (es(Tl)-ea) (Pa)
gbw <- as.numeric(df$calc_gbw) # Boundary layer conductance for water (mol*m-2*s-1)
Cp <- 29.3  # Specific heat capacity of air  (J*mol-1*K-1)
PC <- 0.067 # Psychometric constant "gamma" (kPa*K-1)
PC <- PC *1000   # kPa/K to Pa/K

calc_L <-function (gbw, VPD_Pa, Cp=29.3, PC=67){
  
  #Calculate the Latent heat flux 
  #with formula: L = ((gbw*Cp)/ Gamma)*(es(Tl)- ea) 
  
  L <- ((gbw*Cp)/PC)*VPD_Pa ############################################################################
  results_L <- ("L"= L)
  return(L)
}

#### Loop L per hour ####

L <- NA
for (h in 1:nrow(df)) {
  results_L <- calc_L(gbw[h], VPD_Pa[h], Cp=29.3, PC=67) # Loop for L per hour
  L[h] <- results_L
}
df[, "L"] <- L

#### Outgoing short wave radiation from leaf (W*m–2) ####

sw_in <- as.numeric(df$SW_IN_Wm.2) # Incoming short wave radiation (W*m-2)
albedo <- 0.13 # Albedo of a beech tree leaf (Bonn et al. 2020) (unitless)

calc_sw_out_leaf <- function(sw_in, albedo=0.13){
  # Calculate the outgoing swr
  sw_out_leaf <- albedo*(sw_in)
  results_sw_out_leaf <- ("sw_out_leaf"= sw_out_leaf)
  return(results_sw_out_leaf)
  results_sw_out_leaf
}

#### Loop sw_out_leaf per hour ####

sw_out_leaf <- NA

for (h in 1:nrow(df)) {
  results_sw_out_leaf <- calc_sw_out_leaf(sw_in[h], albedo=0.13) # Loop for sw_out_leaf per hour
  sw_out_leaf[h] <- results_sw_out_leaf
}
df[, "sw_out_leaf"] <- sw_out_leaf

#### Outgoing long wave radiation from leaf (W*m–2) ####

Tl <- as.numeric(df$Tl_K) # Leaf surface temperature (K)
LE <- 0.98 # Leaf emissivity (Unitless)
SBC <- 5.67*10**-8 # Stefan bolzmann constant (W*m-2*K-4)

calc_lw_out_leaf <- function(Tl, LE= 0.98, SBC= 5.67*10**-8){
  
  # Stefan Bolzmann law
  #lw_out_leaf <- 2 * LE * SBC * Tl**4 ??????????????????????????????????????????????????????????????????????????
  lw_out_leaf <- 2* LE * SBC * Tl**4
  results_lw_out_leaf <- ("lw_out_leaf"= lw_out_leaf)
  return(results_lw_out_leaf)
  results_lw_out_leaf
}

#### Loop lw_out_leaf per hour ####

lw_out_leaf <- NA

for (h in 1:nrow(df)) { #print(h)
  results_lw_out_leaf <- calc_lw_out_leaf(Tl [h], LE= 0.98, SBC= 5.67*10**-8) # Loop for lw_out_leaf per hour
  lw_out_leaf[h] <- results_lw_out_leaf
}
df[, "lw_out_leaf"] <- lw_out_leaf

#### Net radiation (Bonan eq. 10.22) (W*m–2) ####

sw_in <- as.numeric(df$SW_IN_Wm.2) # Incoming short wave radiation (W*m-2)
sw_out_leaf <- as.numeric(df$sw_out_leaf) # Outgoing short wave radiation (W*m-2)
lw_in <- as.numeric(df$LW_IN_Wm.2) # Incoming long wave radiation (W*m-2)
lw_out_leaf <- as.numeric(df$lw_out_leaf) # Outgoing long wave radiation (W*m-2)

calc_Rn <- function(sw_in, sw_out_leaf, lw_in, lw_out_leaf){
  
  # Calculate the Net radiation
  Rn <- (sw_in - sw_out_leaf + lw_in - lw_out_leaf) # Maybe after we could include transmitted radiation and soil temperature
  results_Rn <- ("Rn"= Rn)
  return(Rn)
} 

#### Loop Rn per hour ####

Rn <- NA
for (h in 1:nrow(df)) {
  results_Rn <- calc_Rn(sw_in[h], sw_out_leaf[h], lw_in[h], lw_out_leaf[h])  # Loop for Rn per hour
  Rn[h] <- results_Rn
}
df[, "Rn"] <- Rn

#### saturation vapor pressure and derivative (Bonan eq.10.02) #######

#### esat ####

Tl <- as.numeric(df$Tl_DC) # Leaf temperature in degree Celsius !!!

calc_esat <- function(Tl){
  
  # --- For water vapor (temperature range is 0C to 100C)
  
  a0 <-  6.11213476;        b0 <-  0.444017302
  a1 <-  0.444007856;       b1 <-  0.286064092e-01
  a2 <-  0.143064234e-01;   b2 <-  0.794683137e-03
  a3 <-  0.264461437e-03;   b3 <-  0.121211669e-04
  a4 <-  0.305903558e-05;   b4 <-  0.103354611e-06
  a5 <-  0.196237241e-07;   b5 <-  0.404125005e-09
  a6 <-  0.892344772e-10;   b6 <- -0.788037859e-12
  a7 <- -0.373208410e-12;   b7 <- -0.114596802e-13
  a8 <-  0.209339997e-15;   b8 <-  0.381294516e-16
  
  # --- For ice (temperature range is -75C to 0C)
  
  c0 <-  6.11123516;        d0 <-  0.503277922
  c1 <-  0.503109514;       d1 <-  0.377289173e-01
  c2 <-  0.188369801e-01;   d2 <-  0.126801703e-02
  c3 <-  0.420547422e-03;   d3 <-  0.249468427e-04
  c4 <-  0.614396778e-05;   d4 <-  0.313703411e-06
  c5 <-  0.602780717e-07;   d5 <-  0.257180651e-08
  c6 <-  0.387940929e-09;   d6 <-  0.133268878e-10
  c7 <-  0.149436277e-11;   d7 <-  0.394116744e-13
  c8 <-  0.262655803e-14;   d8 <-  0.498070196e-16
 
  # --- Limit temperature to -75C to 100C
  
  Tl <- min(Tl, 100)
  Tl <- max(Tl, -75)
  
  # --- Saturation vapor pressure (esat, mb) and derivative (desat, mb)
  
  ifelse (Tl >= 0,{
    esat  <- a0 + Tl*(a1 + Tl*(a2 + Tl*(a3 + Tl*(a4 + Tl*(a5 + Tl*(a6 + Tl*(a7 + Tl*a8)))))))
    #desat <- b0 + Tl*(b1 + Tl*(b2 + Tl*(b3 + Tl*(b4 + Tl*(b5 + Tl*(b6 + Tl*(b7 + Tl*b8)))))))
  } , 
  {
    esat  <- c0 + Tl*(c1 + Tl*(c2 + Tl*(c3 + Tl*(c4 + Tl*(c5 + Tl*(c6 + Tl*(c7 + Tl*c8)))))))
    #desat <- d0 + Tl*(d1 + Tl*(d2 + Tl*(d3 + Tl*(d4 + Tl*(d5 + Tl*(d6 + Tl*(d7 + Tl*d8)))))))
  })
  
  # --- Convert from mb to Pa
  
  esat  <- esat  * 100
  #desat <- desat * 100
  
  results_esat <- ("esat"= esat)
  return(esat)
}

#### Loop for esat in Pa ####

esat<- NA 

for (h in 1:nrow(df)){
  
  results_esat <- calc_esat(Tl[h]) # Loop for esat per hour
  
  esat[h] <- results_esat # calling the function for esat for each hour<<
}
df[,"esat"] <- esat

#### desat ####

Tl <- as.numeric(df$Tl_DC) # Leaf temperature in degree Celsius !!!

calc_desat <- function(Tl){
  
  # --- For water vapor (temperature range is 0C to 100C)
  
  a0 <-  6.11213476;        b0 <-  0.444017302
  a1 <-  0.444007856;       b1 <-  0.286064092e-01
  a2 <-  0.143064234e-01;   b2 <-  0.794683137e-03
  a3 <-  0.264461437e-03;   b3 <-  0.121211669e-04
  a4 <-  0.305903558e-05;   b4 <-  0.103354611e-06
  a5 <-  0.196237241e-07;   b5 <-  0.404125005e-09
  a6 <-  0.892344772e-10;   b6 <- -0.788037859e-12
  a7 <- -0.373208410e-12;   b7 <- -0.114596802e-13
  a8 <-  0.209339997e-15;   b8 <-  0.381294516e-16
  
  # --- For ice (temperature range is -75C to 0C)
  
  c0 <-  6.11123516;        d0 <-  0.503277922
  c1 <-  0.503109514;       d1 <-  0.377289173e-01
  c2 <-  0.188369801e-01;   d2 <-  0.126801703e-02
  c3 <-  0.420547422e-03;   d3 <-  0.249468427e-04
  c4 <-  0.614396778e-05;   d4 <-  0.313703411e-06
  c5 <-  0.602780717e-07;   d5 <-  0.257180651e-08
  c6 <-  0.387940929e-09;   d6 <-  0.133268878e-10
  c7 <-  0.149436277e-11;   d7 <-  0.394116744e-13
  c8 <-  0.262655803e-14;   d8 <-  0.498070196e-16
  
  # --- Limit temperature to -75C to 100C
  
  Tl <- min(Tl, 100)
  Tl <- max(Tl, -75)
  
  # --- Saturation vapor pressure (esat, mb) and derivative (desat, mb)
  
  ifelse (Tl >= 0,{
    #esat  <- a0 + Tl*(a1 + Tl*(a2 + Tl*(a3 + Tl*(a4 + Tl*(a5 + Tl*(a6 + Tl*(a7 + Tl*a8)))))))
    desat <- b0 + Tl*(b1 + Tl*(b2 + Tl*(b3 + Tl*(b4 + Tl*(b5 + Tl*(b6 + Tl*(b7 + Tl*b8)))))))
  }, {
    #esat  <- c0 + Tl*(c1 + Tl*(c2 + Tl*(c3 + Tl*(c4 + Tl*(c5 + Tl*(c6 + Tl*(c7 + Tl*c8)))))))
    desat <- d0 + Tl*(d1 + Tl*(d2 + Tl*(d3 + Tl*(d4 + Tl*(d5 + Tl*(d6 + Tl*(d7 + Tl*d8)))))))
  })
  
  # --- Convert from mb to Pa
  
  #esat  <- esat  * 100
  desat <- desat * 100
  
  results_desat <- ("desat"= desat)
  return(desat)
}

#### Loop for desat in Pa ####

desat<- NA

for (h in 1:nrow(df)){
  
  results_desat <- calc_desat(Tl[h]) # Loop for desat per hour
  
  desat[h] <- results_desat # calling the function for desat for each hour<<
}

df[,"desat"] <- desat

#### Saturation water vapor at leaf temperature (qsat) #### ????????????????????????????????
#??????????????????????????? in mol/mol

P <- as.numeric(df$PRESS_hPa)*100 # Air pressure (Pa) Von hPa in Pa *100
df[,"PRESS_Pa"] <- P
esat <- as.numeric(df$esat) #  Saturation vapour pressure in Pa

calc_qsat <- function(esat, P){
  # formula: qsat = esat / P
  qsat <- esat / P
  results_qsat <- ("qsat"= qsat)
  return(qsat)
}

#### Loop for qsat ####

qsat <- NA
for (h in 1:nrow(df)) {
  results_qsat <- calc_qsat(esat[h], P[h]) # Loop for qsat per hour
  qsat[h] <- results_qsat
}
df[, "qsat"] <- qsat


#### Derivative of qsat respective to the leaf temperature (dqsat) in Pa ####

P <- as.numeric(df$PRESS_hPa)*100 # Air pressure (Pa) Von hPa in Pa *100
desat <- as.numeric(df$desat) # Derivative of e_sat respective to the leaf temperature in Pa

calc_dqsat <- function(desat, P){
  # Formula: dqsat = desat / P
  dqsat <- desat / P
  result_dqsat <- ("dqsat" = dqsat)
  return(dqsat)
}

#### Loop for dqsat ####

dqsat <- NA
for (h in 1:nrow(df)) {
  results_dqsat <- calc_dqsat(desat[h], P[h]) # Loop for qsat per hour
  dqsat[h] <- results_dqsat
}
df[, "dqsat"] <- dqsat

#### Qa ####

sw_in <- as.numeric(df$SW_IN_Wm.2) # Incoming short wave radiation (W*m-2)
sw_out <- as.numeric(df$SW_OUT_Wm.2)# Outgoing short wave radiation (W*m-2)
lw_in <- as.numeric(df$LW_IN_Wm.2)# Incoming long wave radiation (W*m-2)

calc_Qa <- function(sw_in, sw_out, lw_in){
  Qa <- sw_in - sw_out + lw_in
  result_Qa <- ("Qa" = Qa)
  return(Qa)
}

#### Loop for Qa #### 

Qa <- NA
for (h in 1:nrow(df)) {
  results_Qa <- calc_Qa(sw_in[h], sw_out[h], lw_in[h]) # Loop for Qa per hour
  Qa[h] <- results_Qa
}
df[, "Qa"] <- Qa


#### ea ####

esat <- as.numeric(df$esat)
RH <- as.numeric(df$RH_.)*0.01

calc_ea <- function(esat, RH){
    ea <- esat * RH
    results_ea <- ("ea" = ea)
    return(ea)
}

#### Loop for ea ####

ea <- NA
for (h in 1:nrow(df)) {
    results_ea <- calc_ea(esat[h], RH[h]) # Loop for ea per hour
    ea[h] <- results_ea
}
df[,"ea"] <- ea


#### qa ####

qsat <- as.numeric(df$qsat)
RH <- as.numeric(df$RH_.)*0.01

calc_qa <- function(qsat, RH){
  qa <- qsat * RH
  results_qa <- ("qa" = qa)
  return(qa)
}

#### Loop for qa ####

qa <- NA
for (h in 1:nrow(df)) {
  results_qa <- calc_qa(qsat[h], RH[h]) # Loop for qa per hour
  qa[h] <- results_qa
}
df[,"qa"] <- qa

#### Newton-Raphson Iteration ####

#### Function of Tl (Bonan eq. 10.13) ####

sw_in <- as.numeric(df$SW_IN_Wm.2) # Incoming short wave radiation (W*m-2)
sw_out <- as.numeric(df$SW_OUT_Wm.2)# Outgoing short wave radiation (W*m-2)
lw_in <- as.numeric(df$LW_IN_Wm.2)# Incoming long wave radiation (W*m-2)
Qa <- as.numeric(df$Qa)
qsat <- as.numeric(df$qsat)
RH <- as.numeric(df$RH_.)*0.01
qa <- as.numeric(df$qa)
gbh <- as.numeric(df$calc_gbh) # Boundary layer conductance for heat (mol*m-2*s-1)
gbw <- as.numeric(df$calc_gbw) # Boundary layer conductance for water (mol*m-2*s-1)
VPD_Pa <- as.numeric(df$VPD_hPa)*100 # Vapor pressure deficit (Pa) ############################### kPa or Pa???????????
Tl <- as.numeric(df$Tl_K)
Ta <- as.numeric(df$Ta_K) # Air temperature (K)
Cp <- 29.3 # Specific heat capacity of air (J*mol-1*K-1)
LE <- 0.98 # Leaf emissivity (Unitless)
SBC <- 5.67*10**-8 # Stefan bolzmann constant (W*m-2*K-4)
lambda <- 44.4 # Energy flux (KJ*mol-1)

calc_FTl <- function(sw_in, sw_out, lw_in, Qa, gbh, gbw, qsat, qa, Tl, Ta, Cp=29.3, LE=0.98, SBC=5.67*10**-8, lambda=44.4){
  # Calculate the function of leaf temperature
  # F(Tl) = Qa - (2*LE*SBC*Tl^4) - (2*Cp*(Tl-Ta)*gbh) - lambda*[qsat-qa]*glw = 0 
  FTl <- Qa - (2*LE*SBC*Tl**4) - (2*Cp*(Tl-Ta)*gbh) - lambda*(qsat-qa)*gbw # we except the influence of stomatas because of the absence of data. Consequently glw = gbw.
  results_FTl <- ("FTl"= FTl)
  return(FTl)
}

#### Loop for FTl ####

FTl <- NA

for (h in 1:nrow(df)) {
  results_FTl <- calc_FTl(sw_in[h], sw_out[h], lw_in[h], Qa[h], gbh[h], gbw[h],
                          qsat[h], qa[h], Tl[h], Ta[h], Cp=29.3, LE=0.98, 
                          SBC=5.67*10**-8, lambda =44.4) # Loop for sw_out_leaf per hour
  FTl[h] <- results_FTl
}
df[, "FTl"] <- FTl

#### Temperature derivative (dF/dTl) (Bonan eq. 10.15) ####

gbw <- as.numeric(df$calc_gbw) # Boundary layer conductance for water (mol*m-2*s-1)
gbh <- as.numeric(df$calc_gbh) # Boundary layer conductance for heat (mol*m-2*s-1)
dqsat <- as.numeric(df$dqsat)
Tl <- as.numeric(df$Tl_K) # In DC or in K ???????????????????????????????????????????????????????????????????
LE <- 0.98 # Leaf emissivity (Unitless)
SBC <- 5.67*10**-8 # Stefan bolzmann constant (W*m-2*K-4)
Cp <- 29.3 # Specific heat capacity of air (J*mol-1*K-1)
lambda <- 44.4 # Energy flux (KJ*mol-1)

#(dqsat(Tl)/dT): Derivative of the saturation water vapor with respect to leaf temperature.
# qsat(Tl): Saturation water vapor at leaf temperature.

calc_dF_dTl <- function(gbw, gbh, dqsat, Tl, LE=0.98, SBC = 5.67*10**-8, Cp=29.3, lambda=44.4){
  # Calculate the temperature derivative
  # dF / dTl = - (8 * LE * SBC * Tl^3) - (2 * Cp * gbh) - (lambda * glw * (dqsat(Tl)/dT)) # we except the presence of stomatasso glw = gbw
  dF_dTl <- - (8 * LE * SBC * Tl**3) - (2*Cp * gbh) - (lambda * gbw * dqsat) # gitlab satvapair, change from pascal to mol by dividing with air pressure
  results_dF_dTl <- ("dF_dTl"= dF_dTl)
  return(dF_dTl)
}

#### Loop for dF_dTl ####

dF_dTL <- NA

for (h in 1:nrow(df)) {
  results_dF_dTl <- calc_dF_dTl (gbw[h], gbh[h], dqsat[h], Tl[h], LE=0.98, SBC =5.67*10**-8, Cp=29.3, lambda=44.4)
  dF_dTL[h] <- results_dF_dTl
}
df[, "dF_dTl"] <- dF_dTL

#### Temperature difference (Bonan eq. 10.14) ####

FTl <- as.numeric(df$FTl)
dF_dTl <- as.numeric(df$dF_dTl)

calc_err_Tl <- function(FTl, dF_dTl){
  # Calculate the temperature derivative
  #delta_Tl = - F(Tl)/(deriv_F/deriv_Tl)
  
  err_Tl <- (- FTl / dF_dTl)
  results_err_Tl <- ("err_Tl"= err_Tl)
  return(err_Tl)
}

#### Loop for err_Tl ####

err_Tl <- NA

for (h in 1:nrow(df)) {
  results_err_Tl <- calc_err_Tl (FTl[h], dF_dTl[h])
  err_Tl[h] <- results_err_Tl
}
df[, "err_Tl"] <- err_Tl

#### delta_Tl ####

calc_delta_Tl <- function(FTl, LC=815) {
  delta_Tl <- FTl/LC
  return(delta_Tl)
}

#### Loop for delta_Tl ####

delta_Tl <- NA
for (h in 1:nrow(df)) {
  delta_Tl[h] <- calc_delta_Tl(FTl[h], LC=815)
}
df[,"delta_Tl"] <- delta_Tl

#### Leaf temperature ####

Tl <- as.numeric(df$Tl_K)
delta_Tl <- as.numeric(df$delta_Tl)

calc_Tl_final <- function(Tl, delta_Tl){
  
  Tl_final <- Tl + delta_Tl
  results_Tl_final <- ("Tl_final" = Tl_final)
  return(Tl_final)
  }

#### Loop Leaf temperature ####

Tl_final <- NA

for (h in 1:nrow(df)) {
  results_Tl_final <- calc_Tl_final(Tl[h], delta_Tl[h])
  Tl_final[h] <- results_Tl_final
}
df[,"Tl_final"] <- Tl_final

#### Loop the iteration ####

new_columns <- c("gbh_iter","gbw_iter","H_iter","L_iter", "sw_out_iter","lw_out_iter", 
                 "Rn_iter","Qa_iter","esat_iter","ea_iter","desat_iter","qsat_iter", "dqsat_iter","qa_iter", 
                 "FTl_iter", "err_Tl_iter", "delta_Tl_iter","Tl_iter")

df[new_columns] <- list((character()))


for (i in 1:nrow(df)) { ###### numbers only for the first itteration 
  niter <- 0 # Number of iterations
  err <- 1 # difference between time steps, e.g. energy imbalance
  Tl_init <- as.numeric(df$Ta_K)[i]   #initial leaf temperature (K)
  
  while (niter <=10 & abs(err) > 0.1) { 
    Tl <- Tl_init
  
    gbh <- (calc_gbh(as.numeric(df$Ta_K)[i], as.numeric(df$PRESS_Pa)[i], 
                     as.numeric(df$WS_ms.1)[i], Tl, d=0.05))
    
    gbw <- (calc_gbw(as.numeric(df$Ta_K)[i], as.numeric(df$PRESS_Pa)[i], 
                     as.numeric(df$WS_ms.1)[i], Tl, d=0.05))
    
    H <- calc_H(Tl, as.numeric(df$Ta_K)[i], gbh, Cp=29.3)
    
    esat <- calc_esat(Tl - 273.15)
    
    ea <- calc_ea(esat, as.numeric(df$RH_.)[i]*0.01)
    
    VPD_Pa <- esat-ea
    
    L <- calc_L(gbw, VPD_Pa, Cp=29.3, PC=67)
    
    sw_out <- calc_sw_out_leaf(as.numeric(df$SW_IN_Wm.2)[i], albedo=0.13)
    
    lw_out <- calc_lw_out_leaf(Tl, LE= 0.98, SBC= 5.67*10**-8)
    
    Rn <- calc_Rn(as.numeric(df$SW_IN_Wm.2)[i], sw_out, as.numeric(df$LW_IN_Wm.2)[i], lw_out)
    
    Qa <- calc_Qa(as.numeric(df$SW_IN_Wm.2)[i], sw_out, as.numeric(df$LW_IN_Wm.2)[i])
    
    desat <- calc_desat(Tl - 273.15)
    
    qsat <- calc_qsat(esat, as.numeric(df$PRESS_Pa)[i])
    
    dqsat <- calc_dqsat(desat, as.numeric(df$PRESS_Pa)[i])
    
    qa <- calc_qa(qsat, as.numeric(df$RH_.)[i]*0.01)
    
    FTl <- calc_FTl(as.numeric(df$SW_IN_Wm.2)[i], sw_out, as.numeric(df$LW_IN_Wm.2)[i], 
                    Qa, gbh, gbw, qsat,qa,Tl, as.numeric(df$Ta_K)[i], Cp=29.3, 
                    LE=0.98, SBC=5.67*10**-8, lambda=44.4) 
    
    dF_dTl <- calc_dF_dTl(gbw, gbh, dqsat, Tl, LE=0.98, SBC = 5.67*10**-8, Cp=29.3, lambda=44.4)
  
    err_Tl<- calc_err_Tl(FTl, dF_dTl)
    
    delta_Tl <- calc_delta_Tl(FTl, LC=815)
    
    Tl <- calc_Tl_final(Tl, delta_Tl)
    
    # save values from previous iteration
    Tl_init <- Tl
    
    # leaf energy balance
    err <- err_Tl   ###(Tl_init - Tl)
    
    # Increment iteration counter
    niter <- niter + 1
    
  }
  
 
  df[,"gbh_iter"][i] <- gbh
  
  df[,"gbw_iter"][i] <- gbw

  df[,"H_iter"][i] <-H

  df[,"L_iter"][i] <- L
  
  df[,"sw_out_iter"][i]<- sw_out

  df[,"lw_out_iter"][i] <- lw_out

  df[,"Rn_iter"][i] <- Rn
  
  df[,"Qa_iter"][i] <- Qa

  df[,"esat_iter"][i] <- esat
  
  df[,"ea_iter"][i] <- ea

  df[,"desat_iter"][i] <- desat
  
  df[,"qsat_iter"][i] <- qsat
  
  df[,"dqsat_iter"][i] <- dqsat
  
  df[,"qa"][i] <- qa
  
  df[,"FTl_iter"][i] <- FTl
  
  df[,"delta_Tl_iter"][i] <- delta_Tl
  
  df[,"err_Tl_iter"][i] <- err_Tl
  
  df[, "Tl_iter"][i] <- Tl
  
  #print(df)
  print(i)
  
  }


