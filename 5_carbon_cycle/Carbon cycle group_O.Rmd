---
title: "Carbon Cycle"
author: "Olivia Chiossone" 
date: "2023-12-06"
output: html_document
---

Data Input

```{r}
Data1 <- Measurements_fluxes_hourly_201601_201712
Data2 <- Measurements_fluxes_hourly_201601_201712_gapfilled
metadata
require(ggplot2)
require(dplyr)
require(tidyr)
require(lubridate)
```
Initial Plots
```{r}
Data2$TIMESTAMP_END <- ymd_hms(Data2$TIMESTAMP_END)
plot(Data2$TIMESTAMP_END, Data2$CO2conc_ppm)

```

