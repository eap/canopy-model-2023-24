---
title: "CalibrationModel"
author: "Olivia Chiossone"
date: "2024-01-30"
output: html_document
---
```{r}
library(nloptr)

require(tidyr)
require(dplyr)
require(lubridate)
require(ggplot2)
library(purrr)
library(tibble)
CarbonData <- Measurements_fluxes_hourly_201601_201712_gapfilled
Weather <- Measurements_meteo_hourly_201601_201712_gapfilled

CarbonData<- merge(CarbonData, Weather, by.y = "TIMESTAMP_END")

CarbonData$GPPg <- 1

for (i in 1:nrow(CarbonData)) {
    CarbonData$GPPg[i] = CarbonData$`GPP_DT_umolm-2s-1`[i]*(12.01*10^-6)*60*60*(-1)
    CarbonData$Resp[i] = CarbonData$`RECO_DT_umolm-2s-1` [i]*(12.01*10^-6)*60*60
        
}#changing the unit to grams of C per M^2*sec^-1
get_day_LAI <- function(time) {
  
  stopifnot(all(is.POSIXct(time)))
  ##  --- This is some complex code to be able to support timesteps smaller than a day
  # convert the days of years in dates and the integer
  start_year <- floor_date(time, "year") #first day of the year
  pars_date <- pars %>% 
    select(leaf_out, leaf_full, leaf_fall, leaf_fall_complete) %>% 
    # convert days to seconds
    map_df(~as_datetime(.x * 86400, origin=start_year) %>% as.integer)
  time <- as.integer(time) # convert to integer for easy comparison
  ## ---
  
  case_when(
    # Winter LAI is 0
    time < pars_date$leaf_out ~ 0,
    # spring LAI has linear increase
    time >= pars_date$leaf_out & time < pars_date$leaf_full ~ {
      ndays <- pars_date$leaf_full - pars_date$leaf_out # n days of the transition
      pars$max_LAI * (time - pars_date$leaf_out) / ndays
    },
    # summer LAI is max
    time >= pars_date$leaf_full & time < pars_date$leaf_fall ~ pars$max_LAI,
    # Autumn LAI is decreasing
    time >= pars_date$leaf_fall & time < pars_date$leaf_fall_complete ~ {
      ndays <- pars_date$leaf_fall_complete - pars_date$leaf_fall # n days of the transition
      pars$max_LAI * (pars_date$leaf_fall_complete - time) / ndays
    },
    # winter again
    time >= pars$leaf_fall_complete ~ 0
  )
}
a1=1 #ratios of plant components
a2=0.3
a3=2.2
a4=0.1
a5=0.3
a41=0.6
a51=0.4
a42=0.57
a52=0.43
a63=1.0
k1=8.42*10^-5 #turnover rates
k2=1.09*10^-5
k3=2.82*10^-6
k4=2.74*10^-4
k5=7.62*10^-5
k6=4.55*10^-5
s1=1.16 #scaling factors
s2=1.00
s3=1.00
s4=0.36
s5=0.36
s6=0.36
r1=0.55
s1=0.05
p1=0.8
e=2.71828
bf=((1+a5)*(1+a1+a3*(1+a2)))^(-1)   
#allo to fine root growth
bfr=a1*bf
#allo to live stem
bls=a3*a4*bf
#allo to dead steam
bds=a3*(1-a4)*bf
#allo to live coarse root
blr=a2*a3*a4*bf
#allo to dead coarse root
bdr=a2*a3*(1-a4)*bf
#allo to growth respiration
brg=a5*(bf+bfr+bls+bds+blr+bdr)

Plant=0
Leaf=0         
Root=4528.646     
Wood=5590.046    
MLitter=425.44702   
SLitter=1092.374   
CWD=963.0264
emissions=0.08770874   
t=1
A1=0.5
GPP<-CarbonData$GPPg
GPP<-rep(GPP, 1)
Prec<-CarbonData$PREC_mm
Prec<-rep(Prec, 1)
Date<-CarbonData$TIMESTAMP_END
#f1 #light, where L is the leaf area index
#f2 #soil moisture above wilting point (from soil water group)





CarbonData$TIMESTAMP_END=as_datetime(CarbonData$TIMESTAMP_END)
Date=as_datetime(CarbonData$TIMESTAMP_END)   

pars <- data.frame(leaf_out = 110, leaf_full=170, leaf_fall=280, leaf_fall_complete=300, max_LAI = 5.1 )

emissions=0

LAI_mod <- get_day_LAI(Date) 


Co2_model <- function(A1,CarbonData){
    
  

for (i in 1:17544) {
    
    if(Prec[i]>0){  
        f2=1  }else{
            f2=0.5 #soil moisture variable
        }
    L <- LAI_mod[i]   
    if(L==0){
        
        MLitter[i] = MLitter[i] + Leaf[i]*a41
        SLitter[i] = SLitter[i] + Leaf[i]*a51
        Leaf[i]=0
    }
    f1=e^(-0.5*L)  
    b2=(r1+p1*(1-f2))/(1+p1*(2-f1-f2)) 
    b3=(s1+p1*(1-f1))/(1+p1*(2-f1-f2))
    Plant[i+1] = A1*GPP[i]        
    Leaf[i+1] = Leaf[i] + Plant[i]*(1-b2-b3) - s1*k1*Leaf[i]#Allocation method 2 (b1=1-b2-b3), plus previous leaf carbon
    Root[i+1] = Root[i] + Plant[i]*b2 - k2*Root[i] 
    Wood[i+1] = Wood[i] + Plant[i]*b3 - k3*Wood[i]
        
    MLitter[i+1] = MLitter[i] + Leaf[i]*a41*s1*k1 + Root[i]*a42*k2 - s4*k4*MLitter[i]
    SLitter[i+1] = SLitter[i] + Leaf[i]*a51*s1*k1 + Root[i]*a52*k2 - s5*k5*SLitter[i]
    CWD[i+1] = CWD[i] + Wood[i]*a63*k3 - s6*k6*CWD[i]
        
    emissions[i+1] = s4*k4*MLitter[i] + s5*k5*SLitter[i] +  s6*k6*CWD[i]
    t[i+1] = t[i] + 1
    
}

Allocations <- data.frame(Plant, Leaf, Root, Wood, t, MLitter, SLitter, CWD, emissions)
#Allocations[87601, 1:8]
}
Allocations <- Co2_model(A1, CarbonData)


```

```{r}
Co2_model1 <- function(A1,CarbonData){
    
  

for (i in 1:17544) { 
    L <- LAI_mod[i]   
    if(L==0){
        Leaf[i]=0
        MLitter[i+1]=MLitter[i]+Leaf[i]*a41
        SLitter[i+1]=SLitter[i]+Leaf[i]*a51
        
    }
        Plant[i+1] = A1*GPP[i] 
        Leaf[i+1] = Leaf[i] + Plant[i]*bf - s1*k1*Leaf[i] # -growth and maintence respiration
        Root[i+1] = Root[i] + Plant[i]*bfr + Plant[i]*blr + Plant[i]*bdr - k2*Root[i] # -growth and maintence respiration
        Wood[i+1] = Wood[i] + Plant[i]*bls + Plant[i]*bds - k3*Wood[i] # -growth and maintence respiration
        MLitter[i+1] = MLitter[i] + Leaf[i]*a41*s1*k1 + Root[i]*a42*k2 - s4*k4*MLitter[i]
        SLitter[i+1] = SLitter[i] + Leaf[i]*a51*s1*k1 + Root[i]*a52*k2 - s5*k5*SLitter[i]
        CWD[i+1] = CWD[i] + Wood[i]*a63*k3 - s6*k6*CWD[i]
        
        emissions[i+1] = s4*k4*MLitter[i] + s5*k5*SLitter[i] +  s6*k6*CWD[i]   
        
        #need feedback, like respiration
        t[i+1] = t[i] + 1
    
}

Allocations <- data.frame(Plant,Leaf, Root, Wood, t, MLitter, CWD, SLitter, emissions)
}
Allocations <- Co2_model1(A1, CarbonData)
```



```{r}
obj_crit <- function(x) {
  t <- CarbonData$TIMESTAMP_END[1:17544]
  Co2_obs <- CarbonData$Resp[1:17544]
  obs <- data.frame(t, Co2_obs)
  
  # Initial values
  Co2_i <- 0
  k_i <- x
  time_i <- seq(1, 17545)
  
  # Model run
  mod_output <- Co2_model(k_i, CarbonData[1:17544])
  
  # Comparison with observations
  sim <- data.frame(t = time_i, Co2 = mod_output)[1:17544,]
 # obs <- obs[obs$t %in% sim$t, ]  # Match observations to model output by timestamp
  
  rmse <- sqrt(mean((obs$Co2_obs - sim$Co2.emissions)^2))  # g C/(kg soil)
  return(rmse)
}




mod_calib <- neldermead(A1, obj_crit)


# plot results of model calibration
# initial values
Co2 <- 0
k <- mod_calib$par
time <- seq(1,17544)

# model run
mod_output <- Co2_model1(k, CarbonData[1:17544])
mod_output <- mod_output[1:17544,]
mod_output$t=obs$t
# plot
plot(mod_output$t, mod_output$emissions, col='red')
plot(obs$t, obs$Co2_obs, col='green')
points(mod_output$t, mod_output$emissions, col='red')

sim <- mod_output[obs$t,]
plot(obs$Co2_obs, sim$Co2)
```


