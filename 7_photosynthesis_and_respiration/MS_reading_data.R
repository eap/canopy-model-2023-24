# Maria's script for reading data

##### 1. Importing libraries and setting directory
##### 2. The parameters
### 2.1 All parameters
### 2.2 Temperature independent parameters
### 2.3 Temperature dependent parameters 
##### 3. The data 
### 3.1 Importing Metadata
### 3.2 Directly from measurement
### 3.3 From others #HERE???
##### 4. Plotting
### 4.1 GPP #HERE??? 

# To figure out and do : 
# - check parameters and their units 
#         -> pascals vs. moles ideal gas function?

##### 1. Importing libraries and setting directory ####
library(here)
library(readxl)

setwd(here::here("C:/Users/maria/Rprojects/ecoatmos_model_project/canopy-model-2023-24/measurements"))

##### 2. The parameters ####

### 2.1 All prarameters
all_parameters <- read.csv("parameters.csv")
print(all_parameters[,2])


# usefull for us:
#   o2_atm -> oi?  constant value for oi?

# later maybe for us: 
#   leaf_out
#   leaf_full
#   leaf_fall
#    max_LAI 


new_parameters <- read.csv("parameters_new.csv", sep=";")
print(new_parameters[,c(2,5)])

# [19] "Cp"                
# [20] "Gamma"         Psychometric constant    
# [21] "Sigma"             
# [22] "Lambda"            
# [23] "Epsilon_l"       z  
# [24] "fc"                
# [25] "g"                 
# [26] ""                  
# [27] "R"   Universal gas constant    
# [28] ""                  
# [29] "M_w" Molar density of gases at standard Temp and pressure



# -> We need to add our parameters in the file


### 2.2 Temperature independent parameters # HERE!

# oi <- constant, median or average of o2_atm?  
# g1 <- slope parameter for stomatal conductance models
q1 <-  #HERE???
    # g0 <- minimum stomatal conductance (mol H2O m–2 s–1)
    g0 <-  #HERE???
    
    # alfa_leaf <- leaf absorptance?
    alfa_leaf <- #HERE???
    
    
    ### 2.3 Temperature dependent parameters
    
    # Temperature dependant 
    # esat  <- saturation vapor pressure at temperature
    
    
    # First constant value in 25 C degrees
    
    # Farquhar et al (1980), von Caemmerer (2000) Table 11.2:
# 
# Vmax    maximum Rubisco carboxylation rate and
# Vmax 25 its value at 25C (μmol m–2 s–1) 

vmax_25 <- 2.21

# 
# Jmax maximum electron transport rate and its 
# Jmax_25 value at 25C (μmol m–2 s–1) 
jmax_25 <- 1.65

# Γ∗  CO2 compensation point (μmol mol–1)
# Γ∗25 CO2 compensation point in the absence of non-photorespiratory respiration, and its value at 25C (μmol mol–1)?

comp_CO2_25 <- 1.37 # moles HERE???

# von Caemmerer (1994, 2000) Table 11.1
comp_CO2_25 <- 3.69 # Pa HERE???

# Kc 
kc_25 <- 40.4 #Pa
# Ko
ko_25 <- 24800  #Pa

# Rd heterotrophic respiration 
rd_25 <- #HERE???
    
    ##### 3. The data ####

### 3.1 Importing Metadata

metadata <- readxl::read_xlsx("metadata.xlsx") 
#  needs readxl

print(metadata[,2:3],n=36) #lenght(metadata) 36 

# 1 CO2conc_ppm          mole fraction CO2                                    
# 2 count_exp            count of soil respiration measurements per hour, derived…
# 3 count_lin            count of soil respiration measurements per hour, derived…
# 4 G_5cm_Wm-2           soil heat flux                                           
# 5 GPP_DT_umolm-2s-1    gross primary production, determined based on Lasslop et…
# 6 H_Wm-2               sensible heat flux                                       
# 7 LE_Wm-2              latent heat flux                                         
# 8 LW_IN_Wm-2           incoming longwave radiation                              
# 9 LW_OUT_Wm-2          outgoing longwave radiation                              
# 10 NEE_DT_umolm-2s-1    net ecosystem CO2 exchange, determined based on Lasslop …
# 11 NEE_umolm-2s-1       net ecosystem CO2 exchange                               
# 12 PPFD_IN_umolm-2s-1   incoming photosynthetic photon flux density              
# 13 PPFD_OUT_umolm-2s-1  outgoing photosynthetic photon flux density              
# 14 PREC_mm              precipitation                                            
# 15 PRESS_hPa            air pressure                                             
# 16 RECO_DT_umolm-2s-1   ecosystem respiration, determined based on Lasslop et al…
# 17 RH_%                 relative humidity                                        
# 18 Rsoil_exp_umolm-2s-1 soil respiration, derived with exp-function              
# 19 Rsoil_lin_umolm-2s-1 soil respiration, derived with lin-function              
# 20 stdv_exp             standard deviation of soil respiration, derived with exp…
# 21 stdv_lin             standard deviation of soil respiration, derived with lin…
# 22 SW_DIF_IN_Wm-2       diffuse shortwave radiation                              
# 23 SW_IN_Wm-2           incoming shortwave radiation                             
# 24 SW_OUT_Wm-2          outgoing shortwave radiation                             
# 25 SWC_16cm_%           volumetric soil water content                            
# 26 SWC_32cm_%           volumetric soil water content                            
# 27 SWC_8cm_%            volumetric soil water content                            
# 28 TA_degC              air temperature                                          
# 29 TS_15cm_degC         soil temperature                                         
# 30 TS_2cm_degC          soil temperature                                         
# 31 TS_30cm_degC         soil temperature                                         
# 32 TS_50cm_degC         soil temperature                                         
# 33 TS_5cm_degC          soil temperature                                         
# 34 VPD_hPa              vapor pressure deficit                                   
# 35 WD_deg               wind direction                                           
# 36 WS_ms-1              wind velocity 




### 3.2 Directly from measurements

# ca - atmospheric CO2 consentration
# -> CO2conc_ppm mole fraction CO2 over canopy


# ea - air vapor pressure (Pa) # HERE???
# -> VPD_hPa vapor pressure deficit <- can calculated form this?
# -> TA_degC air temperature
# -> RH_% relative humidity over canopy
# -> PRESS_hPa air pressure over canopy


### 3.3 From others

# I_par - incoming radiation - from radiative transfer group
# -> before we have that lets use PPFD_IN_umolm-2s-1	
# incoming photosynthetic photon flux density over canopy


# T_leaf - leaf temperature
# -> before we have that lets use TA_degC air temperature 

# g_bw - leaf boundary layer conductance 
# -> WD_deg	wind direction
# -> WS_ms-1	wind velocity
# -> we need the model #HERE???


# For validation ?:  #
# A -> photosyntesis rate (without Rd)

# GPP_DT_umolm-2s-1 gross primary production, determined based on Lasslop et al. (2010)-method


##### 4. Plotting # HERE??? ####

### 4.1 GPP
