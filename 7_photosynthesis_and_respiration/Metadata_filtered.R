#Setting Directory
library(here)

setwd(here::here("~/Documents/Model Course Folder/canopy-model-2023-24/7_photosynthesis_and_respiration/Functions.R"))

#Metadata_filtered
library(here)

fluxes <- Measurements_fluxes_hourly_201601_201712_gapfilled

meteo <- Measurements_meteo_hourly_201601_201712_gapfilled

filtered <- data.frame(fluxes$TIMESTAMP_END,fluxes$CO2conc_ppm, meteo$`PPFD_IN_umolm-2s-1`, meteo$TA_degC, meteo$`RH_%`, meteo$VPD_hPa)


