#Setting Directory
library(here)

setwd(here::here("~/Documents/Model Course Folder/canopy-model-2023-24/7_photosynthesis_and_respiration/Functions.R"))
#Functions
#1 Electron Transport Rate

#2 Light limited CO2 Assimilation Rate
A_l <- function(jmax_25, comp_CO2_25,ci)
    {a = jmax_25/4*(CO2-comp_CO2_25/CO2+2*comp_CO2_25)
    return(a)}

#3 RuBP limited CO2 Assimilation Rate
A_c <- function(vmax_25, comp_CO2_25,ci, ko_25, kc_25, oi)
    { a = (vmax_25*(ci-comp_CO2_25)/(ci+kc_25*(1+oi/ko_25)))
    return(a)}

#4 Change in Assimilation Rate
A_n <- function(A_l, A_c)
    { a = min(A_l,A_c)
    return(a)}

#5 Stomatal conductance 