#Soil Temperature Model 
Sys.setlocale("LC_TIME", "English") #To show months in English for some reason they came up in Greek
#Installing the here package and other useful packges
install.packages("here")
library(here)
library(dplyr)
library(lubridate)
library(ggplot2)
library(purrr)

#Setting directory 
setwd(here::here())

#Reading the data for Soil temperature model
soildata_complete <- read.csv((here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv"))) #Measurements_soil_hourly_201601_201712_gapfilled
meteodata_complete <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")) #Measurements_meteo_hourly_201601_201712_gapfilled
parameters <- read.csv((here::here("measurements/parameters.csv")))
soildata_complete$Air_temp <- meteodata_complete$TA_degC #for soil evaporation calculation


# Make sure TIMESTAMP_END is in POSIXct format
soildata_complete$TIMESTAMP_END <- as.POSIXct(soildata_complete$TIMESTAMP_END, format="%Y-%m-%d %H:%M:%S")

# Extracting day, month, and year from the TIMESTAMP_END column
soildata_complete <- soildata_complete %>%
    mutate(Day = format(TIMESTAMP_END, "%d"),
           Month = format(TIMESTAMP_END, "%m"),
           Year = format(TIMESTAMP_END, "%Y"))

# Adding new columns for the average, min, and max temperature and As for each day
soildata_complete <- soildata_complete %>%
    group_by(Year, Month, Day) %>%
    mutate(Avg_Temp_Day = mean(Air_temp, na.rm = TRUE),
           Min_Temp_Day = min(Air_temp, na.rm = TRUE),
           Max_Temp_Day = max(Air_temp, na.rm = TRUE)) %>%
    ungroup() %>%
    group_by(Year, Month, Day) %>%
    mutate(As = (Max_Temp_Day - Min_Temp_Day) / 2) %>%
    ungroup()

# Function to calcualte the LAI 
get_day_LAI <- function(time, pars) {
    
    stopifnot(all(is.POSIXct(time)))
    ##  --- This is some complex code to be able to support timesteps smaller than a day
    # convert the days of years in dates and the integer
    start_year <- floor_date(time, "year") #first day of the year
    pars_date <- pars %>% 
        select(leaf_out, leaf_full, leaf_fall, leaf_fall_complete) %>% 
        # convert days to seconds
        map_df(~as_datetime(.x * 86400, origin=start_year) %>% as.integer)
    time <- as.integer(time) # convert to integer for easy comparison
    ## ---
    
    case_when(
        # Winter LAI is 0
        time < pars_date$leaf_out ~ 0,
        # spring LAI has linear increase
        time >= pars_date$leaf_out & time < pars_date$leaf_full ~ {
            ndays <- pars_date$leaf_full - pars_date$leaf_out # n days of the transition
            pars$max_LAI * (time - pars_date$leaf_out) / ndays
        },
        # summer LAI is max
        time >= pars_date$leaf_full & time < pars_date$leaf_fall ~ pars$max_LAI,
        # Autumn LAI is decreasing
        time >= pars_date$leaf_fall & time < pars_date$leaf_fall_complete ~ {
            ndays <- pars_date$leaf_fall_complete - pars_date$leaf_fall # n days of the transition
            pars$max_LAI * (pars_date$leaf_fall_complete - time) / ndays
        },
        # winter again
        time >= pars$leaf_fall_complete ~ 0
    )
}

# Parameters for the calculation
pars <- data.frame(
    leaf_out = 110,
    leaf_full = 170,
    leaf_fall = 280,
    leaf_fall_complete = 300,
    max_LAI = 5.1
)

# Make sure TIMESTAMP_END is in POSIXct format
soildata_complete$TIMESTAMP_END <- as.POSIXct(soildata_complete$TIMESTAMP_END, format="%Y-%m-%d %H:%M:%S")

# Apply the get_day_LAI function to the data
soildata_complete <- soildata_complete %>%
    mutate(LAI_hourly = get_day_LAI(TIMESTAMP_END, pars))

# Calculates Ts for each day with limiting the negative temperatures and positive values
hours_in_one_day <- 24

soildata_complete <- soildata_complete %>%
    mutate(
        t = ((seq(1, nrow(soildata_complete)) - 1) %% hours_in_one_day) + 1,  # Restart t every 24 hours
        Ts = ifelse(Air_temp < 0, (Air_temp + As * sin(2 * pi * (t - 1) / hours_in_one_day) * exp(-0.5 * LAI_hourly)) * 0.5,
                    ifelse(Air_temp > 25, (Air_temp + As * sin(2 * pi * (t - 1) / hours_in_one_day) * exp(-0.5 * LAI_hourly)) * 0.5,
                           Air_temp + As * sin(2 * pi * (t - 1) / hours_in_one_day) * exp(-0.5 * LAI_hourly))
        )
    )

#=====================================================================================================================================
# Temperature for 2 years Model vs Reality 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_2cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature throughout the 2 Years",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("Model" = "red", "Real Data" = "black"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    theme_minimal()


# Temperature for the 3 week of (January 2016)  Model vs Reality 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_2cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature January 2016",
         x = "Date",
         y = "Temperature") +
    xlim(soildata_complete$TIMESTAMP_END[336],soildata_complete$TIMESTAMP_END[503])



# Temperature for 1 Month(Cold)(January 2016) Model vs Reality 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_2cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature January 2016",
         x = "Date",
         y = "Temperature") +
    xlim(soildata_complete$TIMESTAMP_END[1],soildata_complete$TIMESTAMP_END[719])


# Temperature for 1 Month(Hot)(August 2016) Model vs Reality 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_2cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature August 2016",
         x = "Date",
         y = "Temperature") +
    xlim(soildata_complete$TIMESTAMP_END[5112],soildata_complete$TIMESTAMP_END[5855])

#Temperature for one day 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts_2cm_degC, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_5cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_15cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) + 
    geom_line(aes(y = TS_30cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature August 2016",
         x = "Date",
         y = "Temperature") +
    xlim(soildata_complete$TIMESTAMP_END[5448],soildata_complete$TIMESTAMP_END[5498])

#------------------------------------------------------------------------------
# Temperature of other layers
D = 0.0703
z = 5
P = 24
soildata_complete <- soildata_complete %>%
    mutate(
        t = ((seq(1, nrow(soildata_complete)) - 1) %% hours_in_one_day) + 1,  # Restart t every 24 hours
        Ts5 = Ts + As * exp(-z / D) * sin(2 * pi * (t / P) - (z / D))
    )
# Temperature at 5cm 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts5, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_5cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature throughout the 2 Years",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("Model" = "red", "Real Data" = "black"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    theme_minimal()

    
D = 0.0703
z = 15
P = 24
soildata_complete <- soildata_complete %>%
    mutate(
        t = ((seq(1, nrow(soildata_complete)) - 1) %% hours_in_one_day) + 1,  # Restart t every 24 hours
        Ts15 = Ts-(Ts*0.3) + As * exp(-z / D) * sin(2 * pi * (t / P) - (z / D))
    )

# Temperature at 15cm 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts15, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_15cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature throughout the 2 Years",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("Model" = "red", "Real Data" = "black"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    theme_minimal()


D = 0.0703
z = 30
P = 24
soildata_complete <- soildata_complete %>%
    mutate(
        t = ((seq(1, nrow(soildata_complete)) - 1) %% hours_in_one_day) + 1,  # Restart t every 24 hours
        Ts30 = Ts-(Ts*0.65) + As * exp(-z / D) * sin(2 * pi * t / P - z / D)
    )
# Temperature at 30cm  
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts30, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_30cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature throughout the 2 Years",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("Model" = "red", "Real Data" = "black"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    theme_minimal()

# Temperature for all soil layers from data 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = TS_2cm_degC, color = "2"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_5cm_degC, color = "5"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_15cm_degC, color = "15"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_30cm_degC, color = "30"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_50cm_degC, color = "50"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Temperature for all soil layers from data ",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("2" = "red", "5" = "black","15"="green","30"="pink","50"="orange"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    xlim(soildata_complete$TIMESTAMP_END[5448],soildata_complete$TIMESTAMP_END[5498])
    theme_minimal()
































