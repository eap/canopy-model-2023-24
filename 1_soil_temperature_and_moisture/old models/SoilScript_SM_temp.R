#### open file ####
##install packages 
install.packages("here")
install.packages("ggplot2")

##load libraries 
library(here)
library(dplyr)
library(ggplot2)
library(lubridate)
library(ggplot2)

setwd(here::here())
source(here::here("utility/canopy_parameters.R"))

##load data sets 
soildata_16_17 <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv")) #Measurements_soil_hourly_201601_201712_gapfilled
meteodata_16_17 <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")) #Measurements_meteo_hourly_201601_201712_gapfilled
parameters_1 <- read.csv((here::here("measurements/parameters.csv")))
parameters_2 <- read.csv((here::here("measurements/parameters_new.csv")))

soildata_16_17$TIMESTAMP_END <-as.POSIXct(soildata_16_17$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1') #Correct any errors in timestamp

##Adding necessary data to soildata_16_17 from other data sets
soildata_16_17$PREC_mm <- meteodata_16_17$PREC_mm #for soil moisture calculation
soildata_16_17$Air_temp <- meteodata_16_17$TA_degC #for soil evaporation calculation
####


####################################################
#### 1. Bucket model based on 8 cm (Silty clay) ####
####################################################

temp_gc <- 0.8 # mol/m^2*s # conductivity
# soil depth = 8 cm = 80 mm
# sand 2-3 %, silt 46 %, clay 52-51 % -> silty clay
# Soil moisture difference = R - E - I (+R)

## Temporary variables 
# evaporation rate = 0.0 : "E" in bucket model equation, need to be measured
# soil hydraulic conductivity (8 cm, Silty clay) = 0.05 cm/h = 0.5 mm/h
# infiltration rate (8 cm, silty clay) = 1 mm/h
# Runoff = 0
# Effective porosity (8 cm, silty clay) = 0.423 cm^3 / cm^3

# Assume : soil area = 1 cm^2 = 100 mm^2
# Soil volume = 80 * 100 = 8000 mm^3
# Soil effective pore[mm] = 80 * 0.423 = 33.84 mm
cp <- 2.93000e+01 # J/mol*K # specific heat of moist air 
lambda <-  2.45 #MJ/kg
soil_area <- 100 # mm^2
soil_hydraulic_conductivity <- 0.5 # mm/h
infiltration_rate_8 <- 1 # mm/h
SWC_limit_8 <- 33.84 # mm


## Net Radiation
net_rad <- rep(NA, nrow(soildata_16_17))
net_rad[1] <- 0

for (i in 1:nrow(soildata_16_17)) {
    net_radiation <- meteodata_16_17$LW_IN_Wm.2[i] - meteodata_16_17$LW_OUT_Wm.2[i] + 
        meteodata_16_17$SW_IN_Wm.2[i] - meteodata_16_17$SW_OUT_Wm.2[i]
    net_rad[i] <- net_radiation
}
soildata_16_17$net_rad <- net_rad # W/m^2

## Soil radiation
soil_rad <- rep(NA, nrow(soildata_16_17))
soil_rad[1] <- 0

# daily LAI
soildata_16_17$Daily_LAI <- get_day_LAI(soildata_16_17$TIMESTAMP_END)
for(i in 1:nrow(soildata_16_17)){
    soil_radiation <- soildata_16_17$net_rad[i] * exp(-0.6 * soildata_16_17$Daily_LAI[i])
    soil_rad[i] <- soil_radiation
}
soildata_16_17$soil_rad <- soil_rad # W/m^2


#### EVAPORATION ####

df <- data.frame(time = soildata_16_17$TIMESTAMP_END)

# Gamma [kPa/degC] 
gamma <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    gamma[i] <- 0.665 * 10^-3 * meteodata_16_17$PRESS_hPa[i] / 10
}
df$gamma <- gamma

# Thr, hourly air temperature [degC]
Thr <- meteodata_16_17$TA_degC
Thr[Thr == 0] <- 0.001 # 0 degC doesn't work with exp
df$Thr <- Thr

# Delta [kPa/degC]
delta <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    delta[i] <- (4098 * (0.6108 * exp((17.27 * df$Thr[i]) 
                                      / (df$Thr[i] + 237.3))) / (df$Thr[i] + 237.3)^2)
}
df$delta <- delta

# Rn, net radiation [MJ/m2*hr]
Rn <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    Rn[i] <- soildata_16_17$soil_rad[i] / 10^6 * 60 * 60 # W/m2 to MJ/m2*hr
}
df$Rn <- Rn

# G, soil heat flux density [MJ/m2*hr]
G <- soildata_16_17$G_5cm_Wm.2 / 10^6 * 60 * 60 # W/m2 to MJ/m2*hr
df$G <- G

# u2, wind velocity at 2 m [m/s]
u2 <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    u2[i] <- meteodata_16_17$WS_ms.1[i] * 4.87 / log(67.8 * 44 - 5.42)
}
df$u2 <- u2

# VPD, vapor pressure deficit [kPa]
VPD <- meteodata_16_17$VPD_hPa / 10 # hPa to kPa
df$VPD <- VPD

# Hourly evaporation
ex_evap <- rep(NA, nrow(soildata_16_17))
for (i in 1:nrow(soildata_16_17)) {
    ex_evap[i] <- (0.408 * df$delta[i] * (Rn[i] - G[i]) + df$gamma[i] * 37 / (df$Thr[i] + 273.15) * df$u2[i] * df$VPD[i]) /
        (df$delta[i] + df$gamma[i] * (1 + 0.34 * df$u2[i]))
}
df$evap <- ex_evap

ggplot(df, aes(x = time, y = evap)) +
    geom_line() +
    labs(title = "Soil Evaporation over Time")

soildata_16_17$soil_evap <- ex_evap 

## Precipitation base on LAI
soil_prec <- rep(NA, nrow(soildata_16_17))

for (i in 1:nrow(soildata_16_17)) {
    if (soildata_16_17$Daily_LAI[i] == 0) {
        soil_prec[i] <- soildata_16_17$PREC_mm[i]
    }
    else if (soildata_16_17$Daily_LAI[i] > 0 || soildata_16_17$Daily_LAI[i] < 2.5) {
        soil_prec[i] <- soildata_16_17$PREC_mm[i] * 0.5
    }
    else {
        soil_prec[i] <- soildata_16_17$PREC_mm[i] * 0.1
    }
}
soildata_16_17$hourly_Precipitation <- soil_prec
soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*0.08 + 0.3)

## Visualization 
ggplot(soildata_16_17, aes(x = TIMESTAMP_END, y = soil_evap)) +
    geom_line() +
    labs(title = "Soil Evaporation over Time")

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = PREC_mm, color = "PREC")) + 
    geom_line(aes(y = hourly_Precipitation, color = "hourly")) +
    geom_line(aes(y = prec_total, color = "modulate"))

##melting factor
melt_fact <- rep(NA, nrow(soildata_16_17))
melt_fact[i] <- 0

for (i in 1:nrow(soildata_16_17)){
if (soildata_16_17$Air_temp[i] >= 5) {
    melt_fact[i] <- 0
}
else if (soildata_16_17$Air_temp[i] <= 0) {
    melt_fact[i] <- 0
}
else {
    melt_fact[i] <- 1
}
}
soildata_16_17$Melting <- melt_fact 


###########################
#### 2. Multiple layer ####
###########################

## Soil moisture
soil_moisture_8 <- rep(NA, nrow(soildata_16_17))
soil_moisture_8[1] <- 39.648 / 100 * 80
infiltraion_from_8 <- rep(NA, nrow(soildata_16_17))
infiltraion_from_8[1] <- 0
runoff <- 2.3

# For 8cm soil column 
for (i in 2:nrow(soildata_16_17)) {
    if (soildata_16_17$prec_total[i] >= 3) {
        if (soil_moisture_8[i-1] >= SWC_limit_8 * 0.9) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
                soildata_16_17$soil_evap[i-1] - infiltration_rate_8 - runoff
            infiltraion_from_8[i] <- infiltration_rate_8
        }
        else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.45)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -runoff
            infiltraion_from_8[i] <- 0
        }
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
                soildata_16_17$soil_evap[i-1] -runoff # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0 }
    }
    else {if (soil_moisture_8[i-1] >= SWC_limit_8 * 0.9) { # Saturation limit (max)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
            soildata_16_17$soil_evap[i-1] * 0.8 - infiltration_rate_8 
        infiltraion_from_8[i] <- infiltration_rate_8
    }
        else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.45)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] 
            infiltraion_from_8[i] <- 0
        } 
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] - 
                soildata_16_17$soil_evap[i-1] * 0.8 # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0
        }
    }
}

soildata_16_17$Soil_Moisture_8cm_test <- soil_moisture_8
soildata_16_17$Infiltration_8cm <- infiltraion_from_8

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_8cm_test, color = "Soil_Moisture_8cm")) + 
    geom_line(aes(y = SWC_8cm_. / 100 * 80, color = "SWC_8cm (actual)")) + # % to mm
    geom_line(aes(y = Air_temp / 100 * 80, color = "Air_temp")) +
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 8cm test") +
    scale_color_manual(values = c("black", "red", "blue")) +
    theme_minimal()


# For 16cm soil column
# sand 3 %, silt 46-47 %, clay 51-50 % -> silty clay
soil_moisture_16 <- rep(NA, nrow(soildata_16_17))
soil_moisture_16[1] <- 38.290 / 100 * 80
infiltraion_from_16 <- rep(NA, nrow(soildata_16_17))
infiltraion_from_16[1] <- 0

# all *1.5 and /1.5 : calibration
for (i in 2:nrow(soildata_16_17)) {
    if (soil_moisture_16[i-1] >= SWC_limit_8 ) { 
        soil_moisture_16[i] <- soil_moisture_16[i-1] + 1.3 * soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1] * 1.1 - infiltration_rate_8 #Saturation limit (Max)
        infiltraion_from_16[i] <- infiltration_rate_8
    }
    else if (soil_moisture_16[i-1] <= (SWC_limit_8 * 0.5)) {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + 1.3 * soildata_16_17$Infiltration_8cm[i-1]
        infiltraion_from_16[i] <- 0
    }
    else {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + 1.3 * soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1] * 1.1
        infiltraion_from_16[i] <- 0
    }
}
soildata_16_17$Soil_Moisture_16cm_test <- soil_moisture_16 #Update soildata_16_17 table
soildata_16_17$Infiltration_16cm <- infiltraion_from_16 * 0.8

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_16cm_test, color = "Soil_Moisture_16cm")) +
    geom_line(aes(y = SWC_16cm_./ 100 * 80, color = "SWC_16cm (actual)")) +  # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 16cm test") +
    scale_color_manual(values = c("black", "hotpink")) +
    theme_minimal()

# For 32cm soil column
# sand 3 %, silt 35 %, clay 62 % -> clay
infiltration_rate_clay <- 0.3
SWC_limit_clay <- 160 * 0.412
soil_moisture_32 <- rep(NA, nrow(soildata_16_17))
soil_moisture_32[1] <- 47.530 / 100 * 160

for (i in 2:nrow(soildata_16_17)) {
    if (soil_moisture_32[i-1] >= SWC_limit_clay * 1.1) { 
        soil_moisture_32[i] <- soil_moisture_32[i-1] + soildata_16_17$Infiltration_16cm[i-1] -
            soildata_16_17$soil_evap[i-1] * 0.3 - infiltration_rate_clay #Saturation limit (Max)
    }
    else if (soil_moisture_32[i-1] <= (SWC_limit_clay * 0.8)) {
        soil_moisture_32[i] <- soil_moisture_32[i-1] + soildata_16_17$Infiltration_16cm[i-1]
    }
    else {
        soil_moisture_32[i] <- soil_moisture_32[i-1] + soildata_16_17$Infiltration_16cm[i-1] -
            soildata_16_17$soil_evap[i-1] * 0.3
    }
}
soildata_16_17$Soil_Moisture_32cm_test <- soil_moisture_32

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_32cm_test, color = "Soil_Moisture_32cm")) +
    geom_line(aes(y = SWC_32cm_./ 100 * 160, color = "SWC_32cm (actual)")) +  # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 32cm test") +
    scale_color_manual(values = c("black", "darkgreen")) +
    theme_minimal()

# =================================== #
D = 0.0703
z = 5
P = 24
soildata_complete <- soildata_complete %>%
    mutate(
        t = ((seq(1, nrow(soildata_complete)) - 1) %% hours_in_one_day) + 1,  # Restart t every 24 hours
        Ts5 = sqrt(Ts * 0.7 + As * exp(-z / D) * sin(2 * pi * t / P - z / D))^2
    )

# Temperature for 2 years Model vs Reality 
ggplot(soildata_complete, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Ts5, color = "Model"), linetype = "solid", size = 0.5, alpha = 0.8) +
    geom_line(aes(y = TS_5cm_degC, color = "Real Data"), linetype = "solid", size = 0.5, alpha = 0.8) +
    labs(title = "Soil temperature throughout the 2 Years",
         x = "Date",
         y = "Temperature") +
    scale_color_manual(name=NULL, values = c("Model" = "red", "Real Data" = "black"),
                       labels = c("Model" = "Model", "Real Data" = "Real Data")) +
    theme_minimal()






