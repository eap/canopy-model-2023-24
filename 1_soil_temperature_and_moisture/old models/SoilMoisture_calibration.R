
#################################
#### 8cm Model Function ####
#################################

moisture_model_8 <- function(a, b, c, d, e, f, g, h){
    
    # factor
    runoff <- a #2.29 # 2.3 # runoff rate. mm/h
    infiltration_rate_8 <- b #1.249748 # 1 # infiltration rate of silty clay, mm/h
    SWC_limit_8 <- 80 * 0.423 * c #1.002395 # soil water content limit, mm
    LAI_x <- d #0.13242 # 0.08
    LAI_y <- e #0.2999786 # 0.3
    prec_limit <- f #2.484376 # 2.3
    temp_limit <- g #19.5771645 # 20
    r_factor <- h #1.9428671 # 1
    
    # initial values
    evap_factor_else <- 0.2263753 
    evap_factor_summer <- 1.6889085
    
    # setting
    soil_moisture_8 <- rep(NA, nrow(soildata_16_17))
    soil_moisture_8[1] <- 39.648 / 100 * 80
    infiltraion_from_8 <- rep(NA, nrow(soildata_16_17))
    infiltraion_from_8[1] <- 0
    
    # prec factor
    soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*LAI_x + LAI_y)
    
    # evap factor
    evap_cali <- rep(NA, nrow(soildata_16_17))
    for (i in 1:nrow(soildata_16_17)) {
        if (soildata_16_17$Air_temp[i] < temp_limit) {
            evap_cali[i] <- soildata_16_17$soil_evap[i] * evap_factor_else
        }
        else {
            evap_cali[i] <- soildata_16_17$soil_evap[i] * evap_factor_summer
        }
    }
    soildata_16_17$evap_total <- evap_cali
    
    # calculation
    for (i in 2:nrow(soildata_16_17)) {
        if (soildata_16_17$prec_total[i] >= prec_limit) {
            if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                    soildata_16_17$evap_total[i] - infiltration_rate_8 - runoff
                infiltraion_from_8[i] <- infiltration_rate_8
            }
            else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -runoff * r_factor
                infiltraion_from_8[i] <- 0
            }
            else {
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                    soildata_16_17$evap_total[i] -runoff * r_factor # Calculate change in soil moisture
                infiltraion_from_8[i] <- 0 }
        }
        else {if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] - infiltration_rate_8 
            infiltraion_from_8[i] <- infiltration_rate_8
        }
            else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] 
                infiltraion_from_8[i] <- 0
            } 
            else {
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] - 
                    soildata_16_17$evap_total[i] # Calculate change in soil moisture
                infiltraion_from_8[i] <- 0
            }
        }
    }
    
    # make output nice
    results_8 <- data.frame(time = soildata_16_17$TIMESTAMP_END)
    results_8$Soil_Moisture_8cm <- soil_moisture_8
    results_8$Infiltration_8cm <- infiltraion_from_8
    return(results_8)
}

## calibration
library(nloptr)

obj_crit_8 <- function(parameters){
    
    # initial values
    a <- parameters[1] 
    b <- parameters[2] 
    c <- parameters[3] 
    d <- parameters[4] 
    e <- parameters[5] 
    f <- parameters[6] 
    g <- parameters[7] 
    h <- parameters[8] 
    
    # model run
    mod_output <- moisture_model_8(a, b, c, d, e, f, g, h)
    
    # Comparison with observations
    rmse = sqrt(mean((soildata_16_17$SWC_8cm_.*0.8 - mod_output$Soil_Moisture_8cm)**2))
    return(rmse)
}

# run Nelder-Mead Simplex to minimize RMSE
mod_calib <- neldermead(c(2.29, 1.3, 1, 0.13, 0.3, 2.5, 20, 1), obj_crit_8)

# plot results of model calibration
a <- mod_calib$par[1] # runoff rate, mm/h
b <- mod_calib$par[2] #infiltration rate, mm/h
c <- mod_calib$par[3] # SWC limit at 8cm
d <- mod_calib$par[4] # LAI x factor
e <- mod_calib$par[5] # LAI y factor
f <- mod_calib$par[6] # precipitation limit
g <- mod_calib$par[7] # temperature standard, degC
h <- mod_calib$par[8] # runoff factor

# model run
mod_output <- moisture_model_8(a, b, c, d, e, f, g, h)

# plot
plot(mod_output$time, mod_output$Soil_Moisture_8cm, col='red', typ = "l")
lines(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_8cm_.*0.8, col='black')


#### 8 result ####
a8 <- 4.1100120 # runoff rate, mm/h
b8 <- 1.8942204 # infiltration rate rate, mm/h
c8 <- 0.9388581 # SWC limit at 8cm
d8 <- 0.1911236 # LAI x factor
e8 <- 0.1647810 # LAI y factor
f8 <- 3.3183649 # precipitation limit
g8 <- 23.6979721 # temperature standard, degC
h8 <- 0.7890498 # runoff factor

# model run
mod_output_8 <- moisture_model_8(a8, b8, c8, d8, e8, f8, g8, h8)

# plot
plot(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_8cm_.*0.8, col='red', typ = "l", lwd = 1.5)
lines(mod_output_8$time, mod_output_8$Soil_Moisture_8cm, col='black', lwd = 1.5)



## let's get the new column that we need!!! ##

evap_cali <- numeric(nrow(soildata_16_17))
for (i in 1:nrow(soildata_16_17)) {
    if (soildata_16_17$Air_temp[i] < g8) {
        evap_cali[i] <- soildata_16_17$soil_evap[i] * 0.2263753
    }
    else {
        evap_cali[i] <- soildata_16_17$soil_evap[i] * 1.6889085
    }
}
soildata_16_17$evap_total <- evap_cali

soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*d8 + e8)

soil_moisture_8 <- numeric(nrow(soildata_16_17))
soil_moisture_8[1] <- 39.648 * 0.8
infiltraion_from_8 <- numeric(nrow(soildata_16_17))
for (i in 2:nrow(soildata_16_17)) {
    if (soildata_16_17$prec_total[i] >= f8) {
        if (soil_moisture_8[i-1] >= 80*0.423*c8) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] - b8 - a8
            infiltraion_from_8[i] <- b8
        }
        else if (soil_moisture_8[i-1] <= (80*0.423*c8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -a8 * h8
            infiltraion_from_8[i] <- 0
        }
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] -a8 * h8 # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0 }
    }
    else {if (soil_moisture_8[i-1] >= 80*0.423*c8) { # Saturation limit (max)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
            soildata_16_17$evap_total[i] - b8 
        infiltraion_from_8[i] <- b8
    }
        else if (soil_moisture_8[i-1] <= (80*0.423*c8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] 
            infiltraion_from_8[i] <- 0
        } 
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] - 
                soildata_16_17$evap_total[i] # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0
        }
    }
}
soildata_16_17$Soil_Moisture_8cm <- soil_moisture_8
soildata_16_17$Infiltration_8cm <- infiltraion_from_8




#################################
#### 16cm Model Function ####
#################################

soil_moisture_16 <- numeric(nrow(soildata_16_17))
soil_moisture_16[1] <- 38.290 *0.8
infiltraion_from_16 <- numeric(nrow(soildata_16_17))

moisture_model_16 <- function(aa){
    # factor
    infiltration_factor_from8 <- aa
    
    # initial values
    temp_limit <- 20
    SWC_limit_16 <- 80 * 0.423 # soil water content limit, mm
    evap_factor_summer_16 <- 1.0925270 
    evap_factor_else_16 <- 0.1047889
    infiltration_rate_16 <- 4.508952 
    
    # evap factor
    evap_cali_16 <- rep(NA, nrow(soildata_16_17))
    for (i in 1:nrow(soildata_16_17)) {
        if (soildata_16_17$Air_temp[i] < temp_limit) {
            evap_cali_16[i] <- soildata_16_17$soil_evap[i] * evap_factor_else_16
        }
        else {
            evap_cali_16[i] <- soildata_16_17$soil_evap[i] * evap_factor_summer_16
        }
    }
    soildata_16_17$evap_total_16 <- evap_cali_16
    
    # calculation
    for (i in 2:nrow(soildata_16_17)) {
        if (soil_moisture_16[i-1] >= SWC_limit_8) { 
            soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i] * infiltration_factor_from8-
                soildata_16_17$evap_total_16[i] - infiltration_rate_16 #Saturation limit (Max)
            infiltraion_from_16[i] <- infiltration_rate_16
        }
        else if (soil_moisture_16[i-1] <= (SWC_limit_8 * 0.5)) {
            soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i] * infiltration_factor_from8
            infiltraion_from_16[i] <- 0
        }
        else {
            soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i] * infiltration_factor_from8-
                soildata_16_17$evap_total_16[i]
            infiltraion_from_16[i] <- 0
        }
    }
    soildata_16_17$Soil_Moisture_16cm <- soil_moisture_16 #Update soildata_16_17 table
    soildata_16_17$Infiltration_16cm <- infiltraion_from_16
    
    # make output nice
    results_16 <- data.frame(time = soildata_16_17$TIMESTAMP_END)
    results_16$Soil_Moisture_16cm <- soil_moisture_16
    results_16$Infiltration_16cm <- infiltraion_from_16
    return(results_16)
}

## calibration
library(nloptr)

obj_crit_16 <- function(parameters){
    # initial values
    aa <- parameters #[1] 
    #bb <- parameters[2]
    
    # model run
    mod_output <- moisture_model_16(aa)
    
    # Comparison with observations
    rmse = sqrt(mean((soildata_16_17$SWC_16cm_.*0.8 - mod_output$Soil_Moisture_16cm)**2))
    return(rmse)
}

# run Nelder-Mead Simplex to minimize RMSE
mod_calib <- neldermead(0.8, obj_crit_16)

# plot results of model calibration
aa <- mod_calib$par #[1]
#bb <- mod_calib$par[2]

# model run
mod_output_16 <- moisture_model_16(aa)

# plot
plot(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_16cm_.*0.8, col='hotpink', typ = "l", lwd = 1.5)
lines(mod_output_16$time, mod_output_16$Soil_Moisture_16cm, col='black', lwd = 1.5)


#### 16 result ####
a16 <- 1.600168 # infiltraion factor from 8

# model run
mod_output_16 <- moisture_model_16(a16)

# plot
plot(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_16cm_.*0.8, col='hotpink', typ = "l", lwd = 1.5)
lines(mod_output_16$time, mod_output_16$Soil_Moisture_16cm, col='black', lwd = 1.5)




