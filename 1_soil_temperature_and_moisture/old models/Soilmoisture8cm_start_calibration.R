## cleaned up 

##load libraries 
library(here)
library(dplyr)
library(ggplot2)
library(lubridate)
library(ggplot2)
library(purrr)

setwd(here::here())
source(here::here("utility/canopy_parameters.R"))

##load data sets 
soildata_16_17 <- read.csv(here::here("measurements/Measurements_soil_hourly_201601_201712_gapfilled.csv")) #Measurements_soil_hourly_201601_201712_gapfilled
meteodata_16_17 <- read.csv(here::here("measurements/Measurements_meteo_hourly_201601_201712_gapfilled.csv")) #Measurements_meteo_hourly_201601_201712_gapfilled
parameters_1 <- read.csv((here::here("measurements/parameters.csv")))
parameters_2 <- read.csv((here::here("measurements/parameters_new.csv")))

soildata_16_17$TIMESTAMP_END <-as.POSIXct(soildata_16_17$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1') #Correct any errors in timestamp
meteodata_16_17$TIMESTAMP_END <-as.POSIXct(meteodata_16_17$TIMESTAMP_END, format = "%Y-%m-%d %H:%M:%S", tz='Etc/GMT-1')

##Adding necessary data to soildata_16_17 from other data sets
soildata_16_17$PREC_mm <- meteodata_16_17$PREC_mm #for soil moisture calculation
soildata_16_17$Air_temp <- meteodata_16_17$TA_degC #for soil evaporation calculation
####


####################################################
#### 1. Bucket model based on 8 cm (Silty clay) ####
####################################################

# soil depth = 8 cm = 80 mm
# sand 2-3 %, silt 46 %, clay 52-51 % -> silty clay
# Soil moisture difference = R - E - I (+R)
# infiltration rate (8 cm, silty clay) = 1 mm/h

# Effective porosity (8 cm, silty clay) = 0.423 cm^3 / cm^3
# Soil effective pore[mm] = 80 * 0.423 = 33.84 mm
infiltration_rate_8 <- 1 # mm/h
SWC_limit_8 <- 33.84 # mm


## Net Radiation
net_rad <- rep(NA, nrow(soildata_16_17))
net_rad[1] <- 0

for (i in 1:nrow(soildata_16_17)) {
    net_radiation <- meteodata_16_17$LW_IN_Wm.2[i] - meteodata_16_17$LW_OUT_Wm.2[i] + 
        meteodata_16_17$SW_IN_Wm.2[i] - meteodata_16_17$SW_OUT_Wm.2[i]
    net_rad[i] <- net_radiation
}
soildata_16_17$net_rad <- net_rad # W/m^2

## Soil radiation
soil_rad <- rep(NA, nrow(soildata_16_17))
soil_rad[1] <- 0

# daily LAI
soildata_16_17$Daily_LAI <- get_day_LAI(soildata_16_17$TIMESTAMP_END)
for(i in 1:nrow(soildata_16_17)){
    soil_radiation <- soildata_16_17$net_rad[i] * exp(-0.6 * soildata_16_17$Daily_LAI[i])
    soil_rad[i] <- soil_radiation
}
soildata_16_17$soil_rad <- soil_rad # W/m^2


#### EVAPORATION ####

df <- data.frame(time = soildata_16_17$TIMESTAMP_END)

# Gamma [kPa/degC] 
gamma <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    gamma[i] <- 0.665 * 10^-3 * meteodata_16_17$PRESS_hPa[i] / 10
}
df$gamma <- gamma

# Thr, hourly air temperature [degC]
Thr <- meteodata_16_17$TA_degC
Thr[Thr == 0] <- 0.001 # 0 degC doesn't work with exp
df$Thr <- Thr

# Delta [kPa/degC]
delta <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    delta[i] <- (4098 * (0.6108 * exp((17.27 * df$Thr[i]) 
                                      / (df$Thr[i] + 237.3))) / (df$Thr[i] + 237.3)^2)
}
df$delta <- delta

# Rn, net radiation [MJ/m2*hr]
Rn <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    Rn[i] <- soildata_16_17$soil_rad[i] / 10^6 * 60 * 60 # W/m2 to MJ/m2*hr
}
df$Rn <- Rn

# G, soil heat flux density [MJ/m2*hr]
G <- soildata_16_17$G_5cm_Wm.2 / 10^6 * 60 * 60 # W/m2 to MJ/m2*hr
df$G <- G

# u2, wind velocity at 2 m [m/s]
u2 <- rep(NA, nrow(soildata_16_17))
for(i in 1:nrow(soildata_16_17)){
    u2[i] <- meteodata_16_17$WS_ms.1[i] * 4.87 / log(67.8 * 44 - 5.42)
}
df$u2 <- u2

# VPD, vapor pressure deficit [kPa]
VPD <- meteodata_16_17$VPD_hPa / 10 # hPa to kPa
df$VPD <- VPD

# Hourly evaporation
ex_evap <- rep(NA, nrow(soildata_16_17))
for (i in 1:nrow(soildata_16_17)) {
    ex_evap[i] <- (0.408 * df$delta[i] * (Rn[i] - G[i]) + df$gamma[i] * 37 / (df$Thr[i] + 273.15) * df$u2[i] * df$VPD[i]) /
        (df$delta[i] + df$gamma[i] * (1 + 0.34 * df$u2[i]))
}
df$evap <- ex_evap

ggplot(df, aes(x = time, y = evap)) +
    geom_line() +
    labs(title = "Soil Evaporation over Time")

soildata_16_17$soil_evap <- ex_evap 

## Precipitation base on LAI
soil_prec <- rep(NA, nrow(soildata_16_17))

for (i in 1:nrow(soildata_16_17)) {
    if (soildata_16_17$Daily_LAI[i] == 0) {
        soil_prec[i] <- soildata_16_17$PREC_mm[i]
    }
    else if (soildata_16_17$Daily_LAI[i] > 0 || soildata_16_17$Daily_LAI[i] < 2.5) {
        soil_prec[i] <- soildata_16_17$PREC_mm[i] * 0.5
    }
    else {
        soil_prec[i] <- soildata_16_17$PREC_mm[i] * 0.1
    }
}
soildata_16_17$hourly_Precipitation <- soil_prec
soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*0.08 + 0.3)

## Visualization 
ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = PREC_mm, color = "Prec w/o LAI")) +
    geom_line(aes(y = hourly_Precipitation, color = "Prec w/ LAI")) +
    geom_line(aes(y = prec_total, color = "Prec w/ calibration")) +
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Prec over time") +
    scale_color_manual(values = c("red", "green", "black")) +
    theme_minimal()


###########################
#### 2. Multiple layer ####
###########################

## Soil moisture
soil_moisture_8 <- rep(NA, nrow(soildata_16_17))
soil_moisture_8[1] <- 39.648 / 100 * 80
infiltraion_from_8 <- rep(NA, nrow(soildata_16_17))
infiltraion_from_8[1] <- 0
runoff <- 2.3

# For 8cm soil column 
for (i in 2:nrow(soildata_16_17)) {
    if (soildata_16_17$prec_total[i] >= 3) {
        if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
                soildata_16_17$soil_evap[i-1] - infiltration_rate_8 - runoff
            infiltraion_from_8[i] <- infiltration_rate_8
        }
        else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -runoff
            infiltraion_from_8[i] <- 0
        }
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
                soildata_16_17$soil_evap[i-1] -runoff # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0 }
    }
    else {if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] -
            soildata_16_17$soil_evap[i-1] - infiltration_rate_8 
        infiltraion_from_8[i] <- infiltration_rate_8
    }
        else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] 
            infiltraion_from_8[i] <- 0
        } 
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i-1] - 
                soildata_16_17$soil_evap[i-1] # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0
        }
    }
}

soildata_16_17$Soil_Moisture_8cm <- soil_moisture_8
soildata_16_17$Infiltration_8cm <- infiltraion_from_8

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_8cm, color = "Soil_Moisture_8cm")) + 
    geom_line(aes(y = SWC_8cm_. / 100 * 80, color = "SWC_8cm (actual)")) + # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 8cm") +
    scale_color_manual(values = c("black", "red")) +
    theme_minimal()


##### For 16cm soil column ####
# sand 3 %, silt 46-47 %, clay 51-50 % -> silty clay
soil_moisture_16 <- rep(NA, nrow(soildata_16_17))
soil_moisture_16[1] <- 38.290 / 100 * 80
infiltraion_from_16 <- rep(NA, nrow(soildata_16_17))
infiltraion_from_16[1] <- 0

for (i in 2:nrow(soildata_16_17)) {
    if (soil_moisture_16[i-1] >= SWC_limit_8 ) { 
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1] - infiltration_rate_8 #Saturation limit (Max)
        infiltraion_from_16[i] <- infiltration_rate_8
    }
    else if (soil_moisture_16[i-1] <= (SWC_limit_8 * 0.5)) {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1]
        infiltraion_from_16[i] <- 0
    }
    else {
        soil_moisture_16[i] <- soil_moisture_16[i-1] + soildata_16_17$Infiltration_8cm[i-1] -
            soildata_16_17$soil_evap[i-1]
        infiltraion_from_16[i] <- 0
    }
}
soildata_16_17$Soil_Moisture_16cm <- soil_moisture_16 #Update soildata_16_17 table
soildata_16_17$Infiltration_16cm <- infiltraion_from_16

ggplot(soildata_16_17, aes(x = TIMESTAMP_END)) +
    geom_line(aes(y = Soil_Moisture_16cm, color = "Soil_Moisture_16cm")) +
    geom_line(aes(y = SWC_16cm_.* 0.8, color = "SWC_16cm (actual)")) +  # % to mm
    labs(x = "Date Time", y = "Values", color = "Legend", title = "Soil Moisture Over Time at 16cm") +
    scale_color_manual(values = c("black", "hotpink")) +
    theme_minimal()



#################################
#### 8cm Model Function ####
#################################

moisture_model_8 <- function(a, b, c, d, e, f, g, h){
    
    # factor
    runoff <- a #2.29 # 2.3 # runoff rate. mm/h
    infiltration_rate_8 <- b #1.249748 # 1 # infiltration rate of silty clay, mm/h
    SWC_limit_8 <- 80 * 0.423 * c #1.002395 # soil water content limit, mm
    LAI_x <- d #0.13242 # 0.08
    LAI_y <- e #0.2999786 # 0.3
    prec_limit <- f #2.484376 # 2.3
    temp_limit <- g #19.5771645 # 20
    r_factor <- h #1.9428671 # 1
    
    # initial values
    evap_factor_else <- 0.2263753 
    evap_factor_summer <- 1.6889085
    
    # setting
    soil_moisture_8 <- rep(NA, nrow(soildata_16_17))
    soil_moisture_8[1] <- 39.648 / 100 * 80
    infiltraion_from_8 <- rep(NA, nrow(soildata_16_17))
    infiltraion_from_8[1] <- 0
    
    # prec factor
    soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*LAI_x + LAI_y)
    
    # evap factor
    evap_cali <- rep(NA, nrow(soildata_16_17))
    for (i in 1:nrow(soildata_16_17)) {
        if (soildata_16_17$Air_temp[i] < temp_limit) {
            evap_cali[i] <- soildata_16_17$soil_evap[i] * evap_factor_else
        }
        else {
            evap_cali[i] <- soildata_16_17$soil_evap[i] * evap_factor_summer
        }
    }
    soildata_16_17$evap_total <- evap_cali
    
    # calculation
    for (i in 2:nrow(soildata_16_17)) {
        if (soildata_16_17$prec_total[i] >= prec_limit) {
            if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                    soildata_16_17$evap_total[i] - infiltration_rate_8 - runoff
                infiltraion_from_8[i] <- infiltration_rate_8
            }
            else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -runoff * r_factor
                infiltraion_from_8[i] <- 0
            }
            else {
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                    soildata_16_17$evap_total[i] -runoff * r_factor # Calculate change in soil moisture
                infiltraion_from_8[i] <- 0 }
        }
        else {if (soil_moisture_8[i-1] >= SWC_limit_8) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] - infiltration_rate_8 
            infiltraion_from_8[i] <- infiltration_rate_8
        }
            else if (soil_moisture_8[i-1] <= (SWC_limit_8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] 
                infiltraion_from_8[i] <- 0
            } 
            else {
                soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] - 
                    soildata_16_17$evap_total[i] # Calculate change in soil moisture
                infiltraion_from_8[i] <- 0
            }
        }
    }
    
    # make output nice
    results_8 <- data.frame(time = soildata_16_17$TIMESTAMP_END)
    results_8$Soil_Moisture_8cm <- soil_moisture_8
    results_8$Infiltration_8cm <- infiltraion_from_8
    return(results_8)
}

## calibration
library(nloptr)

obj_crit_8 <- function(parameters){
    
    # initial values
    a <- parameters[1] 
    b <- parameters[2] 
    c <- parameters[3] 
    d <- parameters[4] 
    e <- parameters[5] 
    f <- parameters[6] 
    g <- parameters[7] 
    h <- parameters[8] 
    
    # model run
    mod_output <- moisture_model_8(a, b, c, d, e, f, g, h)
    
    # Comparison with observations
    rmse = sqrt(mean((soildata_16_17$SWC_8cm_.*0.8 - mod_output$Soil_Moisture_8cm)**2))
    return(rmse)
}

# run Nelder-Mead Simplex to minimize RMSE

mod_calib <- neldermead(c(2.29, 1.3, 1, 0.13, 0.3, 2.5, 20, 1), obj_crit_8)

# plot results of model calibration

a <- mod_calib$par[1] # runoff rate, mm/h
b <- mod_calib$par[2] #infiltration rate, mm/h
c <- mod_calib$par[3] # SWC limit at 8cm
d <- mod_calib$par[4] # LAI x factor
e <- mod_calib$par[5] # LAI y factor
f <- mod_calib$par[6] # precipitation limit
g <- mod_calib$par[7] # temperature standard, degC
h <- mod_calib$par[8] # runoff factor

# model run

mod_output <- moisture_model_8(a, b, c, d, e, f, g, h)

# plot

plot(mod_output$time, mod_output$Soil_Moisture_8cm, col='black', typ = "l")
lines(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_8cm_.*0.8, col='red')


#### 8 result ####

a8 <- 4.1100120 # runoff rate, mm/h
b8 <- 1.8942204 # infiltration rate rate, mm/h
c8 <- 0.9388581 # SWC limit at 8cm
d8 <- 0.1911236 # LAI x factor
e8 <- 0.1647810 # LAI y factor
f8 <- 3.3183649 # precipitation limit
g8 <- 23.6979721 # temperature standard, degC
h8 <- 0.7890498 # runoff factor

# model run
mod_output_8 <- moisture_model_8(a8, b8, c8, d8, e8, f8, g8, h8)

# plot
plot(mod_output_8$time, mod_output_8$Soil_Moisture_8cm, col='black', typ = "l")
lines(soildata_16_17$TIMESTAMP_END, soildata_16_17$SWC_8cm_.*0.8, col='red')


## new column ##

evap_cali <- numeric(nrow(soildata_16_17))
for (i in 1:nrow(soildata_16_17)) {
    if (soildata_16_17$Air_temp[i] < g8) {
        evap_cali[i] <- soildata_16_17$soil_evap[i] * 0.2263753
    }
    else {
        evap_cali[i] <- soildata_16_17$soil_evap[i] * 1.6889085
    }
}
soildata_16_17$evap_total <- evap_cali

soildata_16_17$prec_total <- soil_prec * (1 - soildata_16_17$Daily_LAI*d8 + e8)

soil_moisture_8 <- numeric(nrow(soildata_16_17))
soil_moisture_8[1] <- 39.648 * 0.8
infiltraion_from_8 <- numeric(nrow(soildata_16_17))
for (i in 2:nrow(soildata_16_17)) {
    if (soildata_16_17$prec_total[i] >= f8) {
        if (soil_moisture_8[i-1] >= 80*0.423*c8) { # Saturation limit (max)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] - b8 - a8
            infiltraion_from_8[i] <- b8
        }
        else if (soil_moisture_8[i-1] <= (80*0.423*c8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -a8 * h8
            infiltraion_from_8[i] <- 0
        }
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
                soildata_16_17$evap_total[i] -a8 * h8 # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0 }
    }
    else {if (soil_moisture_8[i-1] >= 80*0.423*c8) { # Saturation limit (max)
        soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] -
            soildata_16_17$evap_total[i] - b8 
        infiltraion_from_8[i] <- b8
    }
        else if (soil_moisture_8[i-1] <= (80*0.423*c8 * 0.5)) { # EXAMPLE Field capacity limit (Min)
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] 
            infiltraion_from_8[i] <- 0
        } 
        else {
            soil_moisture_8[i] <- soil_moisture_8[i-1] + soildata_16_17$prec_total[i] - 
                soildata_16_17$evap_total[i] # Calculate change in soil moisture
            infiltraion_from_8[i] <- 0
        }
    }
}
soildata_16_17$Soil_Moisture_8cm <- soil_moisture_8
soildata_16_17$Infiltration_8cm <- infiltraion_from_8




