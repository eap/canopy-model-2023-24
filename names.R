
# ------------------------------------------------------------------------------

# Title: Names Script for Ecosystem - Atmosphere Processes module
# Authors: Eamon O'Keeffe, Florian Wilms 
# Last updated: 09.11.2023

# Description: This scriopt is for all the participants of our class. The names 
# below are the members of the class who have learned how to write, commit, and
# push changes to our gitlab repository. 

# ------------------------------------------------------------------------------

# Names:
# Eamon O'Keeffe
# Moritz
# Yeji
# Verena
# Sudeera
# Gabrielle
# Favour
# Carla
# Gergely
# Konstantinos
# Till
# Effrosyni
# Aaron
# Heidi
# Max
# Maria
# Seunghyun
# Olivia
# Maria
# Charlotte
# Martha
# Sonja
#Megh





# ------------------------------------------------------------------------------