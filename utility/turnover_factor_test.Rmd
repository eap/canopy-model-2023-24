---
title: "Turnover factor change frequency"
output:
  html_document:
    df_print: paged
---

The carbon pool model in the book has a turnover rate that is expressed yearly


The basic equation in the carbon pool model can be simplified in this way:

$$C_{y+1} = C_y(1 - k)$$
where:

- $C_y$ is the carbon at year $y$
- $k$ is the turnover coefficient


The problem is that the turnover rate is for one year and not for one hour (as the model will be run).
However, calculating the new turnover rate for an hourly time step is not trivial, because the rate builds up over time.

The math is the same (at least as far as I can understand) of compound interests, where it is easier to find material and this problem has been already discussed.

From wikipedia page on [compound interest](https://en.wikipedia.org/wiki/Compound_interest)
there is this formula, which seems the one is needed.  
However, I haven't find anywhere a demostration of how this formula has been obtained (hopefully is just a matter of searching better)

$$r_2 = \left[\left(1+\frac{r_1}{n_1}\right)^\frac{n_1}{n_2}-1\right]{n_2}$$

where:

- $r2$ is the new turnover rate
- $r1$ is the old turnover rate
- $n1$ is the frquency for $r1$
- $n2$ is the frequency for $r2$



implementing it in code


```{r}
new_k_freq <- function(k1, n1, n2){
  ((1 + k1/n1) ^ (n1 / n2) -1 ) * n2
}
```


# K for the model

```{r}
# variaous values of k from table 17.3 of the bonan
ks <- c(1.09, 0.1, 0.025, 10, 0.95, .49, 2.15, 0.110, 0.0025)
```

```{r}
new_k_freq(ks, n1 = 1/(365*24), n2 = 1) # hours in a year
```

As a note there are turnover rates which are bigger than one that by themselves are a bit weird (you cannot consume more than what you have), but I assume that in a ecosystem this is compensated by the new inputs


# Simulation

In order to check the values calculated are realistic we make a test to check that a yearly decay rate is equivalent of a hourly decay rate

work with a value of `k = .1` for year

```{r}
library(tidyverse)
```

```{r}
carbon_model <- function(x, k){
  x * (1 - k)
}
```

simulation for one year

```{r}
carbon <- 100
for (i in 1:10){
  carbon[i + 1] <- carbon_model(carbon[i], k = .1)
}
```

```{r}
year <- tibble(
  time = 1:11,
  carbon = carbon
)
```

Now do the same simulation for an hourly rate

```{r}
k_h <- new_k_freq(.1, n1 = 1/(365*24), n2 = 1) # hourly k
```

```{r}
carbon <- 100
for (i in 1:(10*365*24)){
  carbon[i + 1] <- carbon_model(carbon[i], k = k_h)
}
```

```{r}
hour <- tibble(
  time = 1:(10*365*24+1),
  carbon = carbon
)
```

### plot

```{r}
ggplot() +
  geom_line(aes(time*365*24, carbon), year, color='black') +
  geom_line(aes(time, carbon), hour, color='blue')
```

but it is clearly not working


Soooo there is something wrong in the formula


# Optimization process

```{r}
get_cost <- function(k_start) {
  cost <- function(k){
    ## Yearly time rate
    carbon <- 100
    for (i in 1:10){
      carbon[i + 1] <- carbon_model(carbon[i], k = k_start)
    }
    
    year <- tibble(
    time = 1:11,
    carbon = carbon
    )
    ## Hourly time rate
    carbon <- 100
    for (i in 1:(10*365*24)){
      carbon[i + 1] <- carbon_model(carbon[i], k = k)
    }
    
    hour <- tibble(
      time = 1:(10*365*24+1),
      carbon = carbon
    )
    
    hour %>% 
      slice(!!!((1:10)*365*24)) %>% 
      summarize(rmse = (carbon - year$carbon[-1]) ^ 2 %>% mean() %>% sqrt()) %>% 
      pull(rmse)
  }
}
```



Find the minimum

```{r}
library(nloptr)
```


```{r}
k = 10
k_calib <- neldermead(k, get_cost(10))
```

```{r}
k_calib
```


```{r}
carbon <- 100
for (i in 1:(10*365*24)){
  carbon[i + 1] <- carbon_model(carbon[i], k = k_calib$par)
}

hour <- tibble(
  time = 1:(10*365*24+1),
  carbon = carbon
)

ggplot() +
  geom_line(aes((time-1)*365*24, carbon), year, color='black') +
  geom_line(aes(time, carbon), hour, color='blue')
```
For all ks

```{r}
(hourly_ks <- tibble(
  ks = ks,
  hourly_ks = map_dbl(ks, function(k_start){
    k = k_start
    k_calib <- neldermead(k, get_cost(k_start))
    k_calib$par
  }))
)
```


```{r}
hourly_ks
```

