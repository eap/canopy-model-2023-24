---
title: "Canopy Parameters Exploration"
output: html_notebook
---

```{r}
source(here::here("utility/canopy_parameters.R"))
library(tidyverse)
library(here)
theme_set(theme_bw()) # ggplot theme
```

# LAI day

The LAI changes over the year 

```{r}
tibble(
  days = seq(ymd("2021-01-01"), ymd("2021-12-31"), by = "day") %>% as_datetime(),
  LAI = get_day_LAI(days)
) %>% 
  ggplot() +
  geom_line(aes(days, LAI))
```

# LAI Layers

## Canopy profile
```{r}
ggplot() +
  stat_function(fun=canopy_LAI_profile) +
  labs(y="Leaf Area density [m^2 m-3]", x="Relative canopy height") +
  coord_flip() 
```


## LAI distributions layers

```{r}
get_LAI_layers(5, 7) %>% 
  ggplot(aes(n_layer, LAI)) +
  geom_col() +
  coord_flip()
```


Check that the sum of all layers equals to the total LAI
```{r}
get_LAI_layers(5, 7) %>% 
  pull(LAI) %>% 
  sum()
```


 # Delta LAI
 
```{r}
datetime <- c("measurements/Measurements_fluxes_hourly_201601_201712_gapfilled.csv") %>% 
  here() %>% 
  read_csv(show_col_types = FALSE) %>% 
  pull(TIMESTAMP_END)
```
 
```{r}
LAI <- tibble(
  time = datetime,
  LAI = get_day_LAI(datetime),
  delta_LAI = LAI - lag(LAI))
```


```{r}
ggplot(LAI, aes(time, LAI))+
  geom_line()
```

 
```{r}
ggplot(LAI, aes(time, delta_LAI))+
  geom_line()
```

